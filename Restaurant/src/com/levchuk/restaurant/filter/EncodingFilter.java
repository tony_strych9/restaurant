package com.levchuk.restaurant.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


/**
 * A filter to set a language and encoding configurations 
 * in the request before it gets processed.
 * 
 * @author Anton Levchuk
 *
 */
@WebFilter(filterName = "EncodingFilter", urlPatterns = {"/*"}, 
initParams = {@WebInitParam(name="encoding", value="UTF-8"), 
@WebInitParam(name="language", value="en_US")})
public class EncodingFilter implements Filter {

	private String code;
	private String lang;
	
	public void init(FilterConfig fConfig) throws ServletException {
		code = fConfig.getInitParameter("encoding");
		lang = fConfig.getInitParameter("language");
	}
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		String codeRequest = request.getCharacterEncoding();
		if (code != null && !code.equalsIgnoreCase(codeRequest)) {
			request.setCharacterEncoding(code);
			response.setCharacterEncoding(code);
		}
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpSession session = httpRequest.getSession();
		if (session.getAttribute("language") == null) {
			session.setAttribute("language", lang);
		}
		chain.doFilter(request, response);
	}
	
	public void destroy() {
		code = null;
		lang = null;
	}

}
