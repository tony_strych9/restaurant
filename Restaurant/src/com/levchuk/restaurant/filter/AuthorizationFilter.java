package com.levchuk.restaurant.filter;

import java.io.IOException;
import java.util.EnumSet;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.levchuk.restaurant.command.util.CommandEnum;
import com.levchuk.restaurant.util.PageManager;
import com.levchuk.restaurant.util.MessageManager;

/**
 * A filter to redirect a visitor from non-guest resources if he's not authorized.
 * 
 * @author Anton Levchuk
 *
 */
@WebFilter(filterName = "AuthorizationFilter", urlPatterns = {"/jsp/admin/*", "/jsp/client/*", "/controller"})
public class AuthorizationFilter implements Filter {

	private static final String INDEX_PAGE = "index";
	
	/**
	 * A set of commands that are allowed for an unauthorized user.
	 */
	private static final Set<CommandEnum> GUEST_COMMANDS = EnumSet.of(CommandEnum.LOGIN, CommandEnum.SIGNUP, 
			CommandEnum.SHOW_MENU, CommandEnum.SELECT_SECTION, CommandEnum.CHANGE_LANG, 
			CommandEnum.SHOW_DISH_INFO, CommandEnum.UNDEFINED_COMMAND);
	
    public AuthorizationFilter() {}

    public void init(FilterConfig fConfig) throws ServletException {}
    
	public void destroy() {}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		HttpSession session = httpRequest.getSession();
		if (session.getAttribute("user") == null) {
			if (!checkCommand(httpRequest.getParameter("command"))) {
				RequestDispatcher dispatcher = 
						request.getRequestDispatcher(PageManager.getURI(INDEX_PAGE, httpRequest.getMethod()));
				String language = (String) session.getAttribute("language");
				httpRequest.setAttribute("authorizedError", MessageManager.getMessage("error.user.authorized", language));
				dispatcher.forward(httpRequest, httpResponse);
				return;
			}
		}
		chain.doFilter(request, response);
	}

	private boolean checkCommand(String commandName) {
		CommandEnum command = CommandEnum.commandValue(commandName);
		return GUEST_COMMANDS.contains(command);
	}
}
