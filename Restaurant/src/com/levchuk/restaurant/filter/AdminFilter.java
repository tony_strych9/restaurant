package com.levchuk.restaurant.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.levchuk.restaurant.entity.User;
import com.levchuk.restaurant.entity.enums.UserRole;
import com.levchuk.restaurant.util.PageManager;

/**
 * Makes a prohibition of accessing the administrator's resources 
 * by the user that doesn't possess such rights.
 * 
 * @author Anton Levchuk
 *
 */
@WebFilter(filterName = "AdminFilter", urlPatterns = {"/jsp/admin/*"})
public class AdminFilter implements Filter {
	
	private static final String INDEX_PAGE = "index";

    public AdminFilter() {}
    
    public void init(FilterConfig fConfig) throws ServletException {}

	public void destroy() {}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) 
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		if (httpRequest.getSession().getAttribute("user") != null) {
			User user = (User) httpRequest.getSession().getAttribute("user");
			if (user.getRole() != UserRole.ADMINISTRATOR) {
				RequestDispatcher dispatcher = 
						request.getRequestDispatcher(PageManager.getURI(INDEX_PAGE, httpRequest.getMethod()));
				dispatcher.forward(httpRequest, httpResponse);
				return;
			}
		}
		chain.doFilter(request, response);
	}

}
