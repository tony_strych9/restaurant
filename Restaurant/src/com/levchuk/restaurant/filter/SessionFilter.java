package com.levchuk.restaurant.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.levchuk.restaurant.entity.User;
import com.levchuk.restaurant.util.SessionManager;

/**
 * A filter to invalidate user's session if it's not recorded.
 * 
 * @author Anton Levchuk
 *
 */
@WebFilter(filterName = "SessionFilter", urlPatterns = {"/*"})
public class SessionFilter implements Filter {

    public SessionFilter() {}

    public void init(FilterConfig fConfig) throws ServletException {}
    
	public void destroy() {}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpSession session = httpRequest.getSession(false);
		if (session != null) {
			if (session.getAttribute("user") != null) {
				int userID = ((User) session.getAttribute("user")).getUserID();
				SessionManager sm = SessionManager.getSessionManager();
				if (!session.getId().equals(sm.getSessionID(userID))) {
					session.invalidate();
				}
			}
		}
		chain.doFilter(request, response);
	}

}
