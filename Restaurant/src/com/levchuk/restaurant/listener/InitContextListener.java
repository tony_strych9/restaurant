package com.levchuk.restaurant.listener;


import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.xml.DOMConfigurator;

import com.levchuk.restaurant.pool.ConnectionPool;

/**
 * Context listener to initialize parameters during application startup process
 * and destroy not longer needed ones during shutdown.
 */
@WebListener
public class InitContextListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		DOMConfigurator.configure(sce.getServletContext().getRealPath("/config/log4j.xml"));
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		ConnectionPool.getConnectionPool().closePool();
	}

}
