package com.levchuk.restaurant.pool;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

/**
 * ConnectionPool is used to share jdbc connections between users.
 * 
 * @author Anton Levchuk
 *
 */
public class ConnectionPool {

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("resources.database");
	private static final Logger LOG = Logger.getLogger(ConnectionPool.class);
	private static volatile ConnectionPool poolInstance;
	private static AtomicBoolean created = new AtomicBoolean(false);
	private static ReentrantLock singletonLock = new ReentrantLock();
	
	private BlockingQueue<ProxyConnection> pool;

	private ConnectionPool() {
		LOG.debug("Generating connection pool...");
		try {
			DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
			int poolSize = Integer.parseInt(RESOURCE_BUNDLE.getString("poolSize"));
			this.pool = new ArrayBlockingQueue<ProxyConnection>(poolSize);
			for(int i = 0; i < poolSize; i++) {
				ProxyConnection connection = createConnection();
				LOG.debug("Creating connection: " + connection);
				pool.offer(connection);
			}
		} catch (SQLException e) {
			LOG.fatal(e);
			throw new RuntimeException("Error occured during creating a connection pool", e);
		}
	}
	
	public static ConnectionPool getConnectionPool() {
		if (!created.get()) {
			singletonLock.lock();
			try {
				if (!created.get()) {
					poolInstance = new ConnectionPool();
					created.set(true);
				}
			} finally {
				singletonLock.unlock();
			}
		}
		return poolInstance;
	}
	
	public ProxyConnection getConnection() {
		ProxyConnection takenConnection = null;
		try {
			takenConnection = pool.take();
		} catch (InterruptedException e) {
			LOG.error(e);
		}
		return takenConnection;
	}
	
	public boolean releaseConnection(ProxyConnection connection) {
		return pool.offer(connection);
	}

	private ProxyConnection createConnection() throws SQLException {
		ProxyConnection connection = new ProxyConnection(ConnectorDB.getConnection(RESOURCE_BUNDLE));
		return connection;
	}
	
    public void closePool() {
        for (ProxyConnection con : pool) {
        	LOG.debug("Closing connection: " + con);
            try {
            	con.closeConnection();
            } catch (SQLException e) {
                LOG.error(e);
            }
        }
    }
}
