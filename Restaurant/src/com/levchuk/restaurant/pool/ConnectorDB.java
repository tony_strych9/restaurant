package com.levchuk.restaurant.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Manages the creation of a connection.
 * 
 * @author Anton
 *
 */
public final class ConnectorDB {
	
	private ConnectorDB() {}
	
	public static Connection getConnection(ResourceBundle resource) throws SQLException {
		Properties prop = new Properties();
        prop.put("user", resource.getString("user"));
        prop.put("password", resource.getString("password"));
        prop.put("autoReconnect", resource.getString("autoReconnect"));
        prop.put("characterEncoding", resource.getString("encoding"));
        prop.put("useUnicode", resource.getString("useUnicode"));
		return DriverManager.getConnection(resource.getString("url"), prop);
	}
}
