package com.levchuk.restaurant.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.entity.MenuSection;
import com.levchuk.restaurant.pool.ConnectionPool;

/**
 * This class is responsible for processing menu sections' information from a database.
 * 
 * @author Anton Levchuk
 *
 */
public class MenuDAO extends AbstractDAO<Integer, MenuSection>{
	
	private static final Logger LOG = Logger.getLogger(MenuDAO.class);
	
	private static final String GET_SECTIONS = "select SECTION_ID, TITLE from MENU_SECTION";
	private static final String GET_SECTION_BY_ID = "select TITLE from MENU_SECTION "
			+ "where SECTION_ID = ?";
	private static final String CREATE_SECTION = "begin insert into MENU_SECTION "
			+ "values(MENU_SECTION_SEQUENCE.NEXTVAL, ?) returning SECTION_ID into ?; end;";
	private static final String UPDATE_SECTION = "update MENU_SECTION set TITLE = ? where SECTION_ID = ?";
	private static final String DELETE_SECTION = "delete from MENU_SECTION where SECTION_ID = ?";

	/**
	 * Returns a section with the specified ID.
	 * 
	 * @param entityID the section's ID
	 * @return the MenuSection instance
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public MenuSection findByID(int entityID) throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		MenuSection section = null;
		try (PreparedStatement stmt = con.prepareStatement(GET_SECTION_BY_ID)) {
			stmt.setInt(1, entityID);
			ResultSet res = stmt.executeQuery();
			if (res.next()) {
				section = new MenuSection();
				section.setSectionID(entityID);
				section.setTitle(res.getString("TITLE"));
			}
		} catch (SQLException e) {
			throw new DAOException("Error occurred while fetching the section", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return section;
	}

	@Override
	public List<MenuSection> findByCriteria(Integer criteria) throws DAOException {
		ArrayList<MenuSection> sections = new ArrayList<MenuSection>();
		sections.add(findByID(criteria));
		return sections;
	}

	/**
	 * Returns a list of all menu's sections present.
	 * 
	 * @return the list of menu's sections
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public List<MenuSection> findAll() throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		
		ArrayList<MenuSection> sections = new ArrayList<MenuSection>();
		try (Statement stmt = con.createStatement()) {
			ResultSet res = stmt.executeQuery(GET_SECTIONS);
			while(res.next()) {
				MenuSection section = new MenuSection();
				section.setSectionID(res.getInt("SECTION_ID"));
				section.setTitle(res.getString("TITLE"));
				sections.add(section);
			}
		} catch (SQLException e) {
			throw new DAOException("Error occurred while fetching the sections", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return sections;
	}
	
	/**
	 * Creates new menu section's record.
	 * 
	 * @param entity the MenuSection instance
	 * @return ID of newly created section or 0 if the section hasn't been created
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public int create(MenuSection entity) throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		int sectionID = 0;
		try (CallableStatement stmt = con.prepareCall(CREATE_SECTION)) {
			stmt.setString(1, entity.getTitle());
			stmt.registerOutParameter(2, Types.NUMERIC);
			stmt.execute();
			sectionID = stmt.getInt(2);
		} catch (SQLException e) {
			throw new DAOException("Error occurred while creating the section", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return sectionID;
	}

	/**
	 * Updates the section's record.
	 * 
	 * @param entity the MenuSection instance
	 * @return true if changes has been committed, false otherwise
	 * @throws DAOException if an SQLException was thrown during query execution 
	 */
	@Override
	public boolean update(MenuSection entity) throws DAOException {
		boolean result = false;
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		try (PreparedStatement stmt = con.prepareStatement(UPDATE_SECTION)) {
			stmt.setString(1, entity.getTitle());
			stmt.executeUpdate();
			result = true;
		} catch (SQLException e) {
			throw new DAOException("Error occurred while updating the section", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return result;
	}

	/**
	 * Deletes the section's record.
	 * 
	 * @param entityID the section's ID
	 * @return true if the record has been deleted, false otherwise
	 * @throws DAOException if an SQLException was thrown during query execution 
	 */
	@Override
	public boolean deleteByID(int entityID) throws DAOException {
		boolean result = false;
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		try (PreparedStatement stmt = con.prepareStatement(DELETE_SECTION)) {
			stmt.setInt(1, entityID);
			stmt.executeUpdate();
			result = true;
		} catch (SQLException e) {
			throw new DAOException("Error occurred while deleting the section", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return result;
	}

	/**
	 * Deletes the section's record.
	 * 
	 * @param criteria the section's ID
	 * @return true if the record has been deleted, false otherwise
	 * @throws DAOException if an SQLException was thrown during query execution 
	 */
	@Override
	public boolean deleteByCriteria(Integer criteria) throws DAOException {
		return deleteByID(criteria);
	}

}
