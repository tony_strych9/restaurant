package com.levchuk.restaurant.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.entity.Item;
import com.levchuk.restaurant.entity.enums.DishStatus;
import com.levchuk.restaurant.pool.ConnectionPool;

/**
 * This class is responsible for processing items' information from a database.
 * 
 * @author Anton Levchuk
 *
 */
public class ItemDAO extends AbstractDAO<Integer, Item> {
	
	private static final Logger LOG = Logger.getLogger(ItemDAO.class);

	private static final String GET_ITEM = "select ITEM.ORDER_ID, ITEM.DISH_ID, ITEM.QUANTITY,  "
			+ "DISH.DISH_NAME, DISH.PRICE, DISH.STATUS from ITEM join DISH on ITEM.DISH_ID = DISH.DISH_ID "
			+ "where ITEM.ITEM_ID = ?";
	private static final String GET_ALL_ITEMS_BY_ORDER_ID = "select ITEM.ITEM_ID, ITEM.ORDER_ID, ITEM.DISH_ID, ITEM.QUANTITY,  "
			+ "DISH.DISH_NAME, DISH.PRICE, DISH.STATUS from ITEM join DISH on ITEM.DISH_ID = DISH.DISH_ID "
			+ "where ITEM.ORDER_ID = ?";
	private static final String GET_ALL_ITEMS = "select ITEM.ITEM_ID, ITEM.ORDER_ID, ITEM.DISH_ID, ITEM.QUANTITY,  "
			+ "DISH.DISH_NAME, DISH.PRICE, DISH.STATUS from ITEM join DISH on ITEM.DISH_ID = DISH.DISH_ID";
	private static final String CREATE_ITEM = "begin insert into ITEM values(ITEM_SEQUENCE.NEXTVAL, ?, ?, ?) "
			+ "returning ITEM_ID into ?; end;";
	private static final String UPDATE_ITEM = "update ITEM set ORDER_ID = ?, DISH_ID = ?, "
			+ "QUANTITY = ? where ITEM_ID = ?";
	private static final String DELETE_ITEM = "delete from ITEM where ITEM_ID = ?";
	private static final String DELETE_ITEMS_BY_ORDER_ID = "delete from ITEM where ORDER_ID = ?";
	private static final String GET_ITEM_BY_ORDER_ID_AND_DISH_ID = "select ITEM.ITEM_ID, ITEM.QUANTITY, "
			+ "DISH.DISH_NAME, DISH.PRICE, DISH.STATUS from ITEM join DISH on ITEM.DISH_ID = DISH.DISH_ID "
			+ "where ITEM.ORDER_ID = ? and ITEM.DISH_ID = ?";
	
	/**
	 * Returns an item with the specified ID.
	 * 
	 * @param entityID the item's ID
	 * @return the Item instance
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public Item findByID(int entityID) throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		Item item = null;
		try (PreparedStatement stmt = con.prepareStatement(GET_ITEM)) {
			stmt.setInt(1, entityID);
			ResultSet res = stmt.executeQuery();
			if (res.next()) {
				item = new Item();
				item.setItemID(entityID);
				item.setOrderID(res.getInt("ORDER_ID"));
				item.setDishID(res.getInt("DISH_ID"));
				item.setQuantity(res.getInt("QUANTITY"));
				item.setDishName(res.getString("DISH_NAME"));
				item.setDishPrice(res.getDouble("PRICE"));
				item.setDishStatus(DishStatus.valueOf(res.getString("STATUS")));
			}
		} catch (SQLException e) {
			throw new DAOException("Error occurred while fetching the item", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return item;
	}

	/**
	 * Returns a list of items contained in the specified order.
	 * 
	 * @param criteriaID the order's ID
	 * @return the list of items
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public List<Item> findByCriteria(Integer criteria) throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		ArrayList<Item> items = new ArrayList<Item>();
		Item item = null;
		
		try (PreparedStatement stmt = con.prepareStatement(GET_ALL_ITEMS_BY_ORDER_ID)) {
			stmt.setInt(1, criteria);
			ResultSet res = stmt.executeQuery();
			while(res.next()) {
				item = new Item();
				item.setOrderID(criteria);
				item.setItemID(res.getInt("ITEM_ID"));
				item.setDishID(res.getInt("DISH_ID"));
				item.setQuantity(res.getInt("QUANTITY"));
				item.setDishName(res.getString("DISH_NAME"));
				item.setDishPrice(res.getDouble("PRICE"));
				item.setDishStatus(DishStatus.valueOf(res.getString("STATUS")));
				items.add(item);
			}
		} catch (SQLException e) {
			throw new DAOException("Error occurred while fetching the items", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return items;
	}

	/**
	 * Returns a list all items.
	 * 
	 * @return the list of items
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public List<Item> findAll() throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		ArrayList<Item> items = new ArrayList<Item>();
		Item item;
		try (Statement stmt = con.createStatement()) {
			ResultSet res = stmt.executeQuery(GET_ALL_ITEMS);
			while(res.next()) {
				item = new Item();
				item.setItemID(res.getInt("ITEM_ID"));
				item.setOrderID(res.getInt("ORDER_ID"));
				item.setDishID(res.getInt("DISH_ID"));
				item.setQuantity(res.getInt("QUANTITY"));
				item.setDishName(res.getString("DISH_NAME"));
				item.setDishPrice(res.getDouble("PRICE"));
				item.setDishStatus(DishStatus.valueOf(res.getString("STATUS")));
				items.add(item);
			}
		} catch (SQLException e) {
			throw new DAOException("Error occurred while fetching the items", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return items;
	}

	/**
	 * Creates a new item's record.
	 * 
	 * @param entity the Item instance
	 * @return the ID of newly created item or 0 if the item hasn't been created
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public int create(Item entity) throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		int itemID = 0;
		try (CallableStatement stmt = con.prepareCall(CREATE_ITEM)) {
			stmt.setInt(1, entity.getOrderID());
			stmt.setInt(2, entity.getDishID());
			stmt.setInt(3, entity.getQuantity());
			stmt.registerOutParameter(4, Types.NUMERIC);
			stmt.execute();
			itemID = stmt.getInt(4);
		} catch (SQLException e) {
			throw new DAOException("Error occurred while creating the item", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return itemID;
	}

	/**
	 * Updates the item's record.
	 * 
	 * @param entity the Item instance
	 * @return true if changes has been committed, false otherwise
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public boolean update(Item entity) throws DAOException {
		boolean result = false;
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		try (PreparedStatement stmt = con.prepareStatement(UPDATE_ITEM)) {
			stmt.setInt(1, entity.getOrderID());
			stmt.setInt(2, entity.getDishID());
			stmt.setInt(3, entity.getQuantity());
			stmt.setInt(4, entity.getItemID());
			stmt.executeUpdate();
			result = true;
		} catch (SQLException e) {
			throw new DAOException("Error occurred while updating the item", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return result;
	}
	
	/**
	 * Deletes item's record.
	 * 
	 * @param entityID the item's ID
	 * @return true if the record has been deleted, false otherwise
	 * @throws DAOException if an SQLException was thrown during query execution 
	 */
	@Override
	public boolean deleteByID(int entityID) throws DAOException {
		boolean result = false;
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		try (PreparedStatement stmt = con.prepareStatement(DELETE_ITEM)) {
			stmt.setInt(1, entityID);
			stmt.executeUpdate();
			result = true;
		} catch (SQLException e) {
			throw new DAOException("Error occurred while deleting the item", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return result;
	}

	/**
	 * Deletes all dishes contained in the specified order.
	 * 
	 * @param criteria the order's ID
	 * @return true if the record has been deleted, false otherwise
	 * @throws DAOException if an SQLException was thrown during query execution 
	 */
	@Override
	public boolean deleteByCriteria(Integer criteria) throws DAOException {
		boolean result = false;
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		try (PreparedStatement stmt = con.prepareStatement(DELETE_ITEMS_BY_ORDER_ID)) {
			stmt.setInt(1, criteria);
			stmt.executeUpdate();
			result = true;
		} catch (SQLException e) {
			throw new DAOException("Error occurred while deleting the items", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return result;
	}
	
	/**
	 * Returns an item contained in the specified order and having the specified dish.
	 * 
	 * @param orderID the order's ID
	 * @param dishID the dish's ID
	 * @return the Item instance
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	public Item findByOrderIDAndDishID(int orderID, int dishID) throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		Item item = null;
		try (PreparedStatement stmt = con.prepareStatement(GET_ITEM_BY_ORDER_ID_AND_DISH_ID)) {
			stmt.setInt(1, orderID);
			stmt.setInt(2, dishID);
			ResultSet res = stmt.executeQuery();
			if (res.next()) {
				item = new Item();
				item.setItemID(res.getInt("ITEM_ID"));
				item.setOrderID(orderID);
				item.setDishID(dishID);
				item.setQuantity(res.getInt("QUANTITY"));
				item.setDishName(res.getString("DISH_NAME"));
				item.setDishPrice(res.getDouble("PRICE"));
				item.setDishStatus(DishStatus.valueOf(res.getString("STATUS")));
			}
		} catch (SQLException e) {
			throw new DAOException("Error occurred while fetching the item", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return item;
	}
	
}
