package com.levchuk.restaurant.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.entity.Order;
import com.levchuk.restaurant.entity.enums.OrderStatus;
import com.levchuk.restaurant.pool.ConnectionPool;

/**
 * This class is responsible for processing orders' information from a database.
 * 
 * @author Anton Levchuk
 *
 */
public class OrderDAO extends AbstractDAO<Integer, Order>{
	
	private static final Logger LOG = Logger.getLogger(OrderDAO.class);
	
	private static final String GET_ORDER = "select USER_ID, STATUS, ORDER_TIME from ORDERS "
			+ "where ORDER_ID = ?";
	private static final String GET_ORDERS_BY_USER_ID = "select ORDER_ID, STATUS, ORDER_TIME "
			+ "from ORDERS where USER_ID = ?";
	private static final String GET_ALL_ORDERS = "select ORDER_ID, USER_ID, STATUS, ORDER_TIME "
			+ "from ORDERS";
	private static final String CREATE_ORDER = "begin insert into ORDERS values(ORDERS_SEQUENCE.NEXTVAL, ?, ?, ?) "
			+ "returning ORDER_ID into ?; end;";
	private static final String UPDATE_ORDER = "update ORDERS set USER_ID = ?, STATUS = ?, ORDER_TIME = ? where ORDER_ID = ?";
	private static final String DELETE_ORDER = "delete from ORDERS where ORDER_ID = ?";
	private static final String DELETE_ORDERS_BY_USER_ID = "delete from ORDERS where USER_ID = ?";
	private static final String GET_ORDERS_BY_STATUS = "select ORDER_ID, USER_ID, ORDER_TIME "
			+ "from ORDERS where STATUS = ?";
	private static final String UPDATE_ORDER_STATUS = "update ORDERS set STATUS = ? where ORDER_ID = ?";
	
	/**
	 * Returns an order with the specified ID.
	 * 
	 * @param entityID the order's ID
	 * @return the Order instance
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public Order findByID(int entityID) throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		Order order = null;
		try (PreparedStatement stmt = con.prepareStatement(GET_ORDER)) {
			stmt.setInt(1, entityID);
			ResultSet res = stmt.executeQuery();
			if (res.next()) {
				order = new Order();
				order.setOrderID(entityID);
				order.setUserID(res.getInt("USER_ID"));
				order.setStatus(OrderStatus.valueOf(res.getString("STATUS")));
				order.setTime(new Date(res.getTimestamp("ORDER_TIME").getTime()));
			}
		} catch (SQLException e) {
			throw new DAOException("Error occurred while fetching the order", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return order;
	}

	/**
	 * Returns a list of orders bound to the specified user.
	 * 
	 * @param criteria the user's ID
	 * @return the list of orders
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public List<Order> findByCriteria(Integer criteria) throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		Order order = null;
		ArrayList<Order> orders = new ArrayList<Order>();
		try (PreparedStatement stmt = con.prepareStatement(GET_ORDERS_BY_USER_ID)) {
			stmt.setInt(1, criteria);
			ResultSet res = stmt.executeQuery();
			while(res.next()) {
				order = new Order();
				order.setUserID(criteria);
				order.setOrderID(res.getInt("ORDER_ID"));
				order.setStatus(OrderStatus.valueOf(res.getString("STATUS")));
				order.setTime(new Date(res.getTimestamp("ORDER_TIME").getTime()));
				orders.add(order);
			}
		} catch (SQLException e) {
			throw new DAOException("Error occurred while fetching the orders", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return orders;
	}

	/**
	 * Returns a list of all orders.
	 * 
	 * @return the list of orders
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public List<Order> findAll() throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		Order order = null;
		ArrayList<Order> orders = new ArrayList<Order>();
		try (Statement stmt = con.createStatement()) {
			ResultSet res = stmt.executeQuery(GET_ALL_ORDERS);
			while(res.next()) {
				order = new Order();
				order.setOrderID(res.getInt("ORDER_ID"));
				order.setUserID(res.getInt("USER_ID"));
				order.setStatus(OrderStatus.valueOf(res.getString("STATUS")));
				order.setTime(new Date(res.getTimestamp("ORDER_TIME").getTime()));
				orders.add(order);
			}
		} catch (SQLException e) {
			throw new DAOException("Error occurred while fetching the orders", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return orders;
	}

	/**
	 * Creates a new order's record.
	 * 
	 * @param entity the Order instance
	 * @return the ID of newly created order or 0 if the order hasn't been created
	 * @throws DAOException if an SQLException was thrown during query execution 
	 */
	@Override
	public int create(Order entity) throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		int orderID = 0;
		try (CallableStatement stmt = con.prepareCall(CREATE_ORDER)) {
			stmt.setInt(1, entity.getUserID());
			stmt.setString(2, entity.getStatus().name());
			stmt.setTimestamp(3, new Timestamp(entity.getTime().getTime()));
			stmt.registerOutParameter(4, Types.NUMERIC);
			stmt.execute();
			orderID = stmt.getInt(4);
		} catch (SQLException e) {
			throw new DAOException("Error occurred while creating the order", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return orderID;
	}

	/**
	 * Updates order's record.
	 * 
	 * @param entity the Order instance
	 * @return true if changes has been committed, false otherwise
	 * @throws DAOException if an SQLException was thrown during query execution 
	 */
	@Override
	public boolean update(Order entity) throws DAOException {
		boolean result = false;
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		try (PreparedStatement stmt = con.prepareStatement(UPDATE_ORDER)) {
			stmt.setInt(1, entity.getUserID());
			stmt.setString(2, entity.getStatus().name());
			stmt.setTimestamp(3, new Timestamp(entity.getTime().getTime()));
			stmt.setInt(4, entity.getOrderID());
			stmt.executeUpdate();
			result = true;
		} catch (SQLException e) {
			throw new DAOException("Error occurred while updating the order", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return result;
	}
	
	/**
	 * Deletes order's record.
	 * 
	 * @param entityID the order's ID
	 * @return true if the record has been deleted, false otherwise
	 * @throws DAOException if an SQLException was thrown during query execution 
	 */
	@Override
	public boolean deleteByID(int entityID) throws DAOException {
		boolean result = false;
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		try (PreparedStatement stmt = con.prepareStatement(DELETE_ORDER)) {
			stmt.setInt(1, entityID);
			stmt.executeUpdate();
			result = true;
		} catch (SQLException e) {
			throw new DAOException("Error occurred while deleting the order", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return result;
	}

	/**
	 * Deletes a record of an order bound to the specified user.
	 * 
	 * @param criteria the user's ID
	 * @return true if the record has been deleted, false otherwise
	 * @throws DAOException if an SQLException was thrown during query execution 
	 */
	@Override
	public boolean deleteByCriteria(Integer criteria) throws DAOException {
		boolean result = false;
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		try (PreparedStatement stmt = con.prepareStatement(DELETE_ORDERS_BY_USER_ID)) {
			stmt.setInt(1, criteria);
			stmt.executeUpdate();
			result = true;
		} catch (SQLException e) {
			throw new DAOException("Error occurred while deleting the orders", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return result;
	}
	
	/**
	 * Returns a list of orders with the specified status.
	 * 
	 * @param status the order's status
	 * @return the list of orders
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	public List<Order> findByStatus(OrderStatus status) throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		Order order = null;
		ArrayList<Order> orders = new ArrayList<Order>();
		try (PreparedStatement stmt = con.prepareStatement(GET_ORDERS_BY_STATUS)) {
			stmt.setString(1, status.name());
			ResultSet res = stmt.executeQuery();
			while(res.next()) {
				order = new Order();
				order.setOrderID(res.getInt("ORDER_ID"));
				order.setUserID(res.getInt("USER_ID"));
				order.setStatus(status);
				order.setTime(new Date(res.getTimestamp("ORDER_TIME").getTime()));
				orders.add(order);
			}
		} catch (SQLException e) {
			throw new DAOException("Error occurred while fetching the orders", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return orders;
	}
	
	/**
	 * Updates a status of the specified order.
	 * 
	 * @param orderID the order's ID
	 * @param status the order's status
	 * @return true if changes has been committed, false otherwise
	 * @throws DAOException if an SQLException was thrown during query execution 
	 */
	public boolean updateStatus(int orderID, OrderStatus status) throws DAOException {
		boolean result = false;
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		try (PreparedStatement stmt = con.prepareStatement(UPDATE_ORDER_STATUS)) {
			stmt.setString(1, status.name());
			stmt.setInt(2, orderID);
			stmt.executeUpdate();
			result = true;
		} catch (SQLException e) {
			throw new DAOException("Error occurred while updating the order", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return result;
	}

}
