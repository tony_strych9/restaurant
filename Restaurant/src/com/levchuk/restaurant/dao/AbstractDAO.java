package com.levchuk.restaurant.dao;

import java.util.List;

import com.levchuk.restaurant.entity.Entity;

/**
 * This class defines the standard operations to be performed on a model objects.
 * 
 * @author Anton Levchuk
 *
 * @param <T> search criteria type
 * @param <U> entity type
 */
public abstract class AbstractDAO<T, U extends Entity> {

	public abstract U findByID(int entityID) throws DAOException;
	
	public abstract List<U> findByCriteria(T criteria) throws DAOException;
	
	public abstract List<U> findAll() throws DAOException;
	
	public abstract int create(U entity) throws DAOException;
	
	public abstract boolean update(U entity) throws DAOException;
	
	public abstract boolean deleteByID(int entityID) throws DAOException;
	
	public abstract boolean deleteByCriteria(T criteria) throws DAOException;
	
}
