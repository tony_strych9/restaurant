package com.levchuk.restaurant.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.entity.User;
import com.levchuk.restaurant.entity.enums.UserRole;
import com.levchuk.restaurant.pool.ConnectionPool;

/**
 * This class is responsible for processing users' information from a database.
 * 
 * @author Anton Levchuk
 *
 */
public class UserDAO extends AbstractDAO<String, User> {
	
	private static final Logger LOG = Logger.getLogger(UserDAO.class);

	private static final String GET_USER = "select LOGIN, PASSWORD, "
			+ "FIRST_NAME, LAST_NAME, EMAIL, ROLE, LAST_VISIT from USERS where USER_ID = ?";
	private static final String GET_USERS_BY_LOGIN = "select USER_ID, PASSWORD, "
			+ "FIRST_NAME, LAST_NAME, EMAIL, ROLE, LAST_VISIT from USERS where LOGIN = ?";
	private static final String GET_ALL_USERS = "select USER_ID, LOGIN, PASSWORD, "
			+ "FIRST_NAME, LAST_NAME, EMAIL, ROLE, LAST_VISIT from USERS";
	private static final String CREATE_USER = "begin insert into USERS "
			+ "values(USERS_SEQUENCE.NEXTVAL, ?, ?, ?, ?, ?, ?, ?) returning USER_ID into ?; end;";
	private static final String UPDATE_USER = "update USERS set LOGIN = ?, PASSWORD = ?, "
			+ "FIRST_NAME = ?, LAST_NAME = ?, EMAIL = ?, ROLE = ?, LAST_VISIT = ? where USER_ID = ?";
	private static final String DELETE_USER = "delete from USERS where USER_ID = ?";
	private static final String DELETE_USER_BY_LOGIN = "delete from USERS where LOGIN = ?";
	private static final String GET_USER_BY_ORDER_ID = "select USERS.USER_ID, LOGIN, PASSWORD, "
			+ "FIRST_NAME, LAST_NAME, EMAIL, ROLE, LAST_VISIT from USERS join ORDERS "
			+ "on USERS.USER_ID = ORDERS.USER_ID where ORDER_ID = ?";
	private static final String GET_USERS_BY_EMAIL = "select USER_ID, LOGIN, PASSWORD, "
			+ "FIRST_NAME, LAST_NAME, EMAIL, ROLE, LAST_VISIT from USERS where EMAIL = ?";
	
	/**
	 * Returns a user with the specified ID.
	 * 
	 * @param entityID the user's ID
	 * @return the User instance
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public User findByID(int entityID) throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		User user = null;
		try (PreparedStatement stmt = con.prepareStatement(GET_USER)) {
			stmt.setInt(1, entityID);
			ResultSet res = stmt.executeQuery();
			if (res.next()) {
				user = new User();
				user.setUserID(entityID);
				user.setLogin(res.getString("LOGIN"));
				user.setPassword(res.getString("PASSWORD"));
				user.setFirstName(res.getString("FIRST_NAME"));
				user.setLastName(res.getString("LAST_NAME"));
				user.setEmail(res.getString("EMAIL"));
				user.setRole(UserRole.valueOf(res.getString("ROLE")));
				user.setLastVisit(new Date(res.getTimestamp("LAST_VISIT").getTime()));
			}
		} catch (SQLException e) {
			throw new DAOException("Error occurred while fetching the user", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return user;
	}

	/**
	 * Returns a list of users with the specified login.
	 * 
	 * @param criteria the user's login
	 * @return the list of users
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public List<User> findByCriteria(String criteria) throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		ArrayList<User> users = new ArrayList<User>();
		User user = null;
		try (PreparedStatement stmt = con.prepareStatement(GET_USERS_BY_LOGIN)) {
			stmt.setString(1, criteria);
			ResultSet res = stmt.executeQuery();
			while(res.next()) {
				user = new User();
				user.setUserID(res.getInt("USER_ID"));
				user.setLogin(criteria);
				user.setPassword(res.getString("PASSWORD"));
				user.setFirstName(res.getString("FIRST_NAME"));
				user.setLastName(res.getString("LAST_NAME"));
				user.setEmail(res.getString("EMAIL"));
				user.setRole(UserRole.valueOf(res.getString("ROLE")));
				user.setLastVisit(new Date(res.getTimestamp("LAST_VISIT").getTime()));
				users.add(user);
			}
		} catch (SQLException e) {
			throw new DAOException("Error occurred while fetching the users", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return users;
	}

	/**
	 * Returns a list of all users.
	 * 
	 * @return the list of users
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public List<User> findAll() throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		ArrayList<User> users = new ArrayList<User>();
		User user = null;
		try (Statement stmt = con.createStatement()) {
			ResultSet res = stmt.executeQuery(GET_ALL_USERS);
			while(res.next()) {
				user = new User();
				user.setUserID(res.getInt("USER_ID"));
				user.setLogin(res.getString("LOGIN"));
				user.setPassword(res.getString("PASSWORD"));
				user.setFirstName(res.getString("FIRST_NAME"));
				user.setLastName(res.getString("LAST_NAME"));
				user.setEmail(res.getString("EMAIL"));
				user.setRole(UserRole.valueOf(res.getString("ROLE")));
				user.setLastVisit(new Date(res.getTimestamp("LAST_VISIT").getTime()));
				users.add(user);
			}
		} catch (SQLException e) {
			throw new DAOException("Error occurred while fetching the users", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return users;
	}

	/**
	 * Creates new user's record.
	 * 
	 * @param entity the User instance
	 * @return the ID of newly created user or 0 if the user hasn't been created
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public int create(User entity) throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		int clientID = 0;
		try (CallableStatement stmt = con.prepareCall(CREATE_USER)) {
			stmt.setString(1, entity.getLogin());
			stmt.setString(2, entity.getPassword());
			stmt.setString(3, entity.getFirstName());
			stmt.setString(4, entity.getLastName());
			stmt.setString(5, entity.getEmail());
			stmt.setString(6, entity.getRole().name());
			stmt.setTimestamp(7, new Timestamp(entity.getLastVisit().getTime()));
			stmt.registerOutParameter(8, Types.NUMERIC);
			stmt.execute();
			clientID = stmt.getInt(8);
		} catch (SQLException e) {
			throw new DAOException("Error occurred while creating the user", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return clientID;
	}

	/**
	 * Updates user's record.
	 * 
	 * @param entity the User instance
	 * @return true if changes has been committed, false otherwise
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public boolean update(User entity) throws DAOException {
		boolean result = false;
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		try (PreparedStatement stmt = con.prepareStatement(UPDATE_USER)) {
			stmt.setString(1, entity.getLogin());
			stmt.setString(2, entity.getPassword());
			stmt.setString(3, entity.getFirstName());
			stmt.setString(4, entity.getLastName());
			stmt.setString(5, entity.getEmail());
			stmt.setString(6, entity.getRole().name());
			stmt.setTimestamp(7, new Timestamp(entity.getLastVisit().getTime()));
			stmt.setInt(8, entity.getUserID());
			stmt.executeUpdate();
			result = true;
		} catch (SQLException e) {
			throw new DAOException("Error occurred while updating the user", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return result;
	}
	
	/**
	 * Deletes user's record.
	 * 
	 * @param entityID the user's ID
	 * @return true if the record has been deleted, false otherwise
	 * @throws DAOException if an SQLException was thrown during query execution 
	 */
	@Override
	public boolean deleteByID(int entityID) throws DAOException {
		boolean result = false;
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		try (PreparedStatement stmt = con.prepareStatement(DELETE_USER)) {
			stmt.setInt(1, entityID);
			stmt.executeUpdate();
			result = true;
		} catch (SQLException e) {
			throw new DAOException("Error occurred while deleting the user", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return result;
	}

	/**
	 * Deletes records of users with the specified login.
	 * 
	 * @param criteria the user's login
	 * @return true if the records has been deleted, false otherwise
	 * @throws DAOException if an SQLException was thrown during query execution 
	 */
	@Override
	public boolean deleteByCriteria(String criteria) throws DAOException {
		boolean result = false;
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		try (PreparedStatement stmt = con.prepareStatement(DELETE_USER_BY_LOGIN)) {
			stmt.setString(1, criteria);
			stmt.executeUpdate();
			result = true;
		} catch (SQLException e) {
			throw new DAOException("Error occurred while deleting the users", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return result;
	}

	/**
	 * Returns a user bound to the specified order.
	 * 
	 * @param orderID the order's ID
	 * @return the User instance
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	public User findByOrderID(int orderID) throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		User user = null;
		try (PreparedStatement stmt = con.prepareStatement(GET_USER_BY_ORDER_ID)) {
			stmt.setInt(1, orderID);
			ResultSet res = stmt.executeQuery();
			if (res.next()) {
				user = new User();
				user.setUserID(res.getInt("USER_ID"));
				user.setLogin(res.getString("LOGIN"));
				user.setPassword(res.getString("PASSWORD"));
				user.setFirstName(res.getString("FIRST_NAME"));
				user.setLastName(res.getString("LAST_NAME"));
				user.setEmail(res.getString("EMAIL"));
				user.setRole(UserRole.valueOf(res.getString("ROLE")));
				user.setLastVisit(new Date(res.getTimestamp("LAST_VISIT").getTime()));
			}
		} catch (SQLException e) {
			throw new DAOException("Error occurred while fetching the user", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return user;
	}

	/**
	 * Returns a list of users bound to the specified email.
	 * 
	 * @param email the user's email
	 * @return the list of users
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	public List<User> findByEmail(String email) throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		ArrayList<User> users = new ArrayList<User>();
		User user = null;
		try (PreparedStatement stmt = con.prepareStatement(GET_USERS_BY_EMAIL)) {
			stmt.setString(1, email);
			ResultSet res = stmt.executeQuery();
			while(res.next()) {
				user = new User();
				user.setUserID(res.getInt("USER_ID"));
				user.setLogin(res.getString("LOGIN"));
				user.setPassword(res.getString("PASSWORD"));
				user.setFirstName(res.getString("FIRST_NAME"));
				user.setLastName(res.getString("LAST_NAME"));
				user.setEmail(email);
				user.setRole(UserRole.valueOf(res.getString("ROLE")));
				user.setLastVisit(new Date(res.getTimestamp("LAST_VISIT").getTime()));
				users.add(user);
			}
		} catch (SQLException e) {
			throw new DAOException("Error occurred while fetching the users", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return users;
	}
}
