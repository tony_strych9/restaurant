package com.levchuk.restaurant.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.entity.Dish;
import com.levchuk.restaurant.entity.enums.DishStatus;
import com.levchuk.restaurant.pool.ConnectionPool;

/**
 * This class is responsible for processing dishes' information from a database.
 * 
 * @author Anton Levchuk
 *
 */
public class DishDAO extends AbstractDAO<Integer, Dish> {
	
	private static final Logger LOG = Logger.getLogger(DishDAO.class);

	private static final String GET_DISH_BY_ID = "select SECTION_ID, DISH_NAME, PRICE, DESCRIPTION, STATUS, IMAGE "
			+ "from DISH where DISH_ID = ?";
	private static final String GET_DISHES_BY_SECTION_ID = "select DISH_ID, DISH_NAME, PRICE, DESCRIPTION, STATUS, IMAGE "
			+ "from DISH where SECTION_ID = ?";
	private static final String GET_ALL_DISHES = "select DISH_ID, SECTION_ID, DISH_NAME, PRICE, DESCRIPTION, STATUS, IMAGE "
			+ "from DISH";
	private static final String CREATE_DISH = "begin insert into DISH values(DISH_SEQUENCE.NEXTVAL, ?, ?, ?, ?, ?, ?) "
			+ "returning DISH_ID into ?; end;";
	private static final String UPDATE_DISH = "update DISH set SECTION_ID = ?, DISH_NAME = ?, "
			+ "PRICE = ?, DESCRIPTION = ?, STATUS = ?, IMAGE = ? where DISH_ID = ?";
	private static final String DELETE_DISH = "delete from DISH where DISH_ID = ?";
	private static final String DELETE_DISHES_BY_SECTION_ID = "delete from DISH where SECTION_ID = ?";
	private static final String GET_DISHES_BY_ORDER_ID = "select DISH.DISH_ID, SECTION_ID, "
			+ "DISH_NAME, PRICE, DESCRIPTION, STATUS, IMAGE from DISH "
			+ "join ITEM on ITEM.DISH_ID = DISH.DISH_ID "
			+ "where ORDER_ID = ?";
	private static final String GET_DISH_BY_ITEM_ID = "select DISH.DISH_ID, SECTION_ID, "
			+ "DISH_NAME, PRICE, DESCRIPTION, STATUS, IMAGE from DISH "
			+ "join ITEM on ITEM.DISH_ID = DISH.DISH_ID "
			+ "where ITEM_ID = ?";
	private static final String GET_DISH_BY_NAME = "select DISH_ID, SECTION_ID, PRICE, DESCRIPTION, STATUS, IMAGE "
			+ "from DISH where DISH_NAME = ?";
	private static final String UPDATE_DISH_STATUS = "update DISH set STATUS = ? where DISH_ID = ?";
	
	/**
	 * Returns a Dish object representing a dish with specified ID.
	 * 
	 * @param entityID dish's ID
	 * @return the Dish instance
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public Dish findByID(int entityID) throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		Dish dish = null;
		try (PreparedStatement stmt = con.prepareStatement(GET_DISH_BY_ID)) {
			stmt.setInt(1, entityID);
			ResultSet res = stmt.executeQuery();
			if (res.next()) {
				dish = new Dish();
				dish.setDishID(entityID);
				dish.setName(res.getString("DISH_NAME"));
				dish.setSectionID(res.getInt("SECTION_ID"));
				dish.setPrice(res.getDouble("PRICE"));
				dish.setDescription(res.getString("DESCRIPTION"));
				dish.setStatus(DishStatus.valueOf(res.getString("STATUS")));
				dish.setImage(res.getString("IMAGE"));
			}
		} catch (SQLException e) {
			throw new DAOException("Error occurred while fetching the dish", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return dish;
	}

	/**
	 * Returns a list of all dishes contained in the specified menu's section.
	 * 
	 * @param criteria section's ID
	 * @return the list of dishes
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public List<Dish> findByCriteria(Integer criteria) throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		ArrayList<Dish> dishes = new ArrayList<Dish>();
		Dish dish = null;
		try (PreparedStatement stmt = con.prepareStatement(GET_DISHES_BY_SECTION_ID)) {
			stmt.setInt(1, criteria);
			ResultSet res = stmt.executeQuery();
			while(res.next()) {
				dish = new Dish();
				dish.setDishID(res.getInt("DISH_ID"));
				dish.setSectionID(criteria);
				dish.setName(res.getString("DISH_NAME"));
				dish.setPrice(res.getDouble("PRICE"));
				dish.setDescription(res.getString("DESCRIPTION"));
				dish.setStatus(DishStatus.valueOf(res.getString("STATUS")));
				dish.setImage(res.getString("IMAGE"));
				dishes.add(dish);
			}
		} catch (SQLException e) {
			throw new DAOException("Error occurred while fetching the dishes", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return dishes;
	}

	/**
	 * Returns a list of all dishes.
	 * 
	 * @return the list of dishes
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public List<Dish> findAll() throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		ArrayList<Dish> dishes = new ArrayList<Dish>();
		Dish dish;
		try (Statement stmt = con.createStatement()) {
			ResultSet res = stmt.executeQuery(GET_ALL_DISHES);
			while(res.next()) {
				dish = new Dish();
				dish.setDishID(res.getInt("DISH_ID"));
				dish.setSectionID(res.getInt("SECTION_ID"));
				dish.setName(res.getString("DISH_NAME"));
				dish.setPrice(res.getDouble("PRICE"));
				dish.setDescription(res.getString("DESCRIPTION"));
				dish.setStatus(DishStatus.valueOf(res.getString("STATUS")));
				dish.setImage(res.getString("IMAGE"));
				dishes.add(dish);
			}
		} catch (SQLException e) {
			throw new DAOException("Error occurred while fetching the dishes", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return dishes;
	}

	/**
	 * Creates a new dish.
	 * 
	 * @param entity the Dish instance
	 * @return the ID of newly created dish or 0 if the dish hasn't been created
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public int create(Dish entity) throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		int dishID = 0;
		try (CallableStatement stmt = con.prepareCall(CREATE_DISH)) {
			stmt.setInt(1, entity.getSectionID());
			stmt.setString(2, entity.getName());
			stmt.setDouble(3, entity.getPrice());
			stmt.setString(4, entity.getDescription());
			stmt.setString(5, entity.getStatus().name());
			stmt.setString(6, entity.getImage());
			stmt.registerOutParameter(7, Types.NUMERIC);
			stmt.execute();
			dishID = stmt.getInt(7);
		} catch (SQLException e) {
			throw new DAOException("Error occurred while creating the dish", e);
		} finally {
			try {
				
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return dishID;
	}

	/**
	 * Updates dish's record.
	 * 
	 * @param entity the Dish instance
	 * @return true if changes has been committed, false otherwise
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public boolean update(Dish entity) throws DAOException {
		boolean result = false;
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		try (PreparedStatement stmt = con.prepareStatement(UPDATE_DISH)) {
			stmt.setInt(1, entity.getSectionID());
			stmt.setString(2, entity.getName());
			stmt.setDouble(3, entity.getPrice());
			stmt.setString(4, entity.getDescription());
			stmt.setString(5, entity.getStatus().name());
			stmt.setString(6, entity.getImage());
			stmt.setInt(7, entity.getDishID());
			stmt.executeUpdate();
			result = true;
		} catch (SQLException e) {
			throw new DAOException("Error occurred while updating the dish", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return result;
	}

	/**
	 * Deletes dish's record.
	 * 
	 * @param entityID dish's ID
	 * @return true if the record has been deleted, false otherwise
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public boolean deleteByID(int entityID) throws DAOException {
		boolean result = false;
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		try (PreparedStatement stmt = con.prepareStatement(DELETE_DISH)) {
			stmt.setInt(1, entityID);
			stmt.executeUpdate();
			result = true;
		} catch (SQLException e) {
			throw new DAOException("Error occurred while deleting the dish", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return result;
	}
	
	/**
	 * Deletes all dishes present in the specified menu's section.
	 * 
	 * @param criteriaID section's ID
	 * @return true if the record has been deleted, false otherwise
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	@Override
	public boolean deleteByCriteria(Integer criteria) throws DAOException {
		boolean result = false;
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		try (PreparedStatement stmt = con.prepareStatement(DELETE_DISHES_BY_SECTION_ID)) {
			stmt.setInt(1, criteria);
			stmt.executeUpdate();
			result = true;
		} catch (SQLException e) {
			throw new DAOException("Error occurred while deleting the dishes", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return result;
	}

	/**
	 * Returns a list of dishes contained in the specified order.
	 * 
	 * @param orderID the order's ID
	 * @return the list of dishes
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	public List<Dish> findByOrderId(int orderID) throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		ArrayList<Dish> dishes = new ArrayList<Dish>();
		Dish dish = null;
		try (PreparedStatement stmt = con.prepareStatement(GET_DISHES_BY_ORDER_ID)) {
			stmt.setInt(1, orderID);
			ResultSet res = stmt.executeQuery();
			while(res.next()) {
				dish = new Dish();
				dish.setDishID(res.getInt("DISH_ID"));
				dish.setSectionID(res.getInt("SECTION_ID"));
				dish.setName(res.getString("DISH_NAME"));
				dish.setPrice(res.getDouble("PRICE"));
				dish.setDescription(res.getString("DESCRIPTION"));
				dish.setStatus(DishStatus.valueOf(res.getString("STATUS")));
				dish.setImage(res.getString("IMAGE"));
				dishes.add(dish);
			}
		} catch (SQLException e) {
			throw new DAOException("Error occurred while fetching the dishes", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return dishes;
	}
	
	/**
	 * Returns a dish with the specified name.
	 * 
	 * @param name the dish's name
	 * @return the Dish instance
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	public Dish findByName(String name) throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		Dish dish = null;
		try (PreparedStatement stmt = con.prepareStatement(GET_DISH_BY_NAME)) {
			stmt.setString(1, name);
			ResultSet res = stmt.executeQuery();
			if (res.next()) {
				dish = new Dish();
				dish.setDishID(res.getInt("DISH_ID"));
				dish.setSectionID(res.getInt("SECTION_ID"));
				dish.setName(name);
				dish.setPrice(res.getDouble("PRICE"));
				dish.setDescription(res.getString("DESCRIPTION"));
				dish.setStatus(DishStatus.valueOf(res.getString("STATUS")));
				dish.setImage(res.getString("IMAGE"));
			}
		} catch (SQLException e) {
			throw new DAOException("Error occurred while fetching the dish", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return dish;
	}
	
	/**
	 * Updates a status of the specified dish.
	 * 
	 * @param dishID the dish's ID
	 * @param status the dish's status
	 * @return true if changes has been committed, false otherwise
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	public boolean updateStatus(int dishID, DishStatus status) throws DAOException {
		boolean result = false;
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		try (PreparedStatement stmt = con.prepareStatement(UPDATE_DISH_STATUS)) {
			stmt.setString(1, status.name());
			stmt.setInt(2, dishID);
			stmt.executeUpdate();
			result = true;
		} catch (SQLException e) {
			throw new DAOException("Error occurred while updating the dish", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return result;
	}
	
	/**
	 * Returns a dish present in the specified item.
	 * 
	 * @param itemID the item's ID
	 * @return the Dish instance
	 * @throws DAOException if an SQLException was thrown during query execution
	 */
	public Dish findByItemID(int itemID) throws DAOException {
		Connection con = ConnectionPool.getConnectionPool().getConnection();
		Dish dish = null;
		try (PreparedStatement stmt = con.prepareStatement(GET_DISH_BY_ITEM_ID)) {
			stmt.setInt(1, itemID);
			ResultSet res = stmt.executeQuery();
			if (res.next()) {
				dish = new Dish();
				dish.setDishID(res.getInt("DISH_ID"));
				dish.setName(res.getString("DISH_NAME"));
				dish.setSectionID(res.getInt("SECTION_ID"));
				dish.setPrice(res.getDouble("PRICE"));
				dish.setDescription(res.getString("DESCRIPTION"));
				dish.setStatus(DishStatus.valueOf(res.getString("STATUS")));
				dish.setImage(res.getString("IMAGE"));
			}
		} catch (SQLException e) {
			throw new DAOException("Error occurred while fetching the dish", e);
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.error(e);
			}
		}
		return dish;
	}
}
