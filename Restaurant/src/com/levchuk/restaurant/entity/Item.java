package com.levchuk.restaurant.entity;

import com.levchuk.restaurant.entity.enums.DishStatus;

/**
 * An entity representing a single item from the order.
 * 
 * @author Anton Levchuk
 *
 */
public class Item extends Entity {

	private int itemID;
	private int orderID;
	private int dishID;
	private int quantity;
	private String dishName;
	private double dishPrice;
	private DishStatus dishStatus;

	public Item() {}
	
	public int getItemID() {
		return itemID;
	}

	public void setItemID(int itemID) {
		this.itemID = itemID;
	}

	public int getOrderID() {
		return orderID;
	}

	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}

	public void setDishID(int dishID) {
		this.dishID = dishID;
	}
	
	public int getDishID() {
		return dishID;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public String getDishName() {
		return dishName;
	}

	public void setDishName(String dishName) {
		this.dishName = dishName;
	}

	public double getDishPrice() {
		return dishPrice;
	}

	public void setDishPrice(double dishPrice) {
		this.dishPrice = dishPrice;
	}
	
	public double getTotalPrice() {
		return dishPrice * quantity;
	}
	
	public DishStatus getDishStatus() {
		return dishStatus;
	}

	public void setDishStatus(DishStatus dishStatus) {
		this.dishStatus = dishStatus;
	}

}
