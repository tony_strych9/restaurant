package com.levchuk.restaurant.entity;

/**
 * A base class for all entities.
 * 
 * @author Anton Levchuk
 */
public abstract class Entity {}
