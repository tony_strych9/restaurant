package com.levchuk.restaurant.entity;

import java.util.Date;

import com.levchuk.restaurant.entity.enums.UserRole;

/**
 * An entity representing user's information.
 * 
 * @author Anton Levchuk
 *
 */
public class User extends Entity {

	private int userID;
	private String login;
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	private UserRole role;
	private Date lastVisit;
	
	public User() {}
	
	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public Date getLastVisit() {
		return lastVisit;
	}

	public void setLastVisit(Date lastVisit) {
		this.lastVisit = lastVisit;
	}
}
