package com.levchuk.restaurant.entity;

/**
 * An entity representing a single section from the menu.
 * 
 * @author Anton Levchuk
 *
 */
public class MenuSection extends Entity {

	private int sectionID;
	private String title;
	
	public MenuSection() {}
	
	public int getSectionID() {
		return sectionID;
	}

	public void setSectionID(int sectionID) {
		this.sectionID = sectionID;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
}
