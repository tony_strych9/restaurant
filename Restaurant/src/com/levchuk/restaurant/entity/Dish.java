package com.levchuk.restaurant.entity;

import com.levchuk.restaurant.entity.enums.DishStatus;

/**
 * An entity representing a dish from the menu.
 * 
 * @author Anton Levchuk
 *
 */
public class Dish extends Entity {

	private int dishID;
	private int sectionID;
	private String name;
	private double price;
	private String description;
	private DishStatus status;
	private String image;
	
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Dish() {}
	
	public DishStatus getStatus() {
		return status;
	}

	public void setStatus(DishStatus status) {
		this.status = status;
	}

	public int getDishID() {
		return dishID;
	}

	public void setDishID(int dishID) {
		this.dishID = dishID;
	}
	
	public int getSectionID() {
		return sectionID;
	}

	public void setSectionID(int sectionID) {
		this.sectionID = sectionID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
