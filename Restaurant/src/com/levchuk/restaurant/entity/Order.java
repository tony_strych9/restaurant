package com.levchuk.restaurant.entity;

import java.util.Date;

import com.levchuk.restaurant.entity.enums.OrderStatus;

/**
 * An entity representing an order.
 * 
 * @author Anton Levchuk
 *
 */
public class Order extends Entity {

	private int orderID;
	private int userID;
	private OrderStatus status;
	private Date time;
	
	public Order() {}
	
	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public int getOrderID() {
		return orderID;
	}

	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

}
