package com.levchuk.restaurant.entity.enums;

public enum OrderStatus {
	FORMING, SUBMITTED, CONFIRMED, PAID, CANCELED
}
