package com.levchuk.restaurant.entity.enums;

public enum DishStatus {
	ACTIVE, INACTIVE
}
