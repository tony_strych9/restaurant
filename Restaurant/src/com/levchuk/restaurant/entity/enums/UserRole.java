package com.levchuk.restaurant.entity.enums;

public enum UserRole {
	CLIENT, ADMINISTRATOR
}
