package com.levchuk.restaurant.util;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Utility class that provides a control on users' sessions.
 * 
 * @author Anton Levchuk
 *
 */
public class SessionManager {

	/**
	 * A map for storing user's ID/session's ID pairs.
	 */
	private ConcurrentHashMap<Integer, String> sessions;
	
	private static class SessionManagerHolder {
		private static final SessionManager INSTANCE = new SessionManager();
	}
	
	private SessionManager() {
		sessions = new ConcurrentHashMap<Integer, String>();
	}
	
	public static SessionManager getSessionManager() {
		return SessionManagerHolder.INSTANCE;
	}
	
	/**
	 * Returns the session's ID bound to the specified user.
	 * 
	 * @param userID user's ID
	 * @return the session's ID
	 */
	public String getSessionID(int userID) {
		String sessionID;
		sessionID = sessions.get(userID);
		return sessionID;
	}
	
	/**
	 * Adds the session's ID bound to the specified user.
	 * 
	 * @param userID the user's ID
	 * @param sessionID the session's ID
	 */
	public void addSession(int userID, String sessionID) {
		sessions.put(userID, sessionID);
	}
	
	/**
	 * Remove a session bound to the specified user.
	 * 
	 * @param userID the user's ID
	 */
	public void removeSession(int userID) {
		sessions.remove(userID);
	}
	
}
