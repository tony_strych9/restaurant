package com.levchuk.restaurant.util;

import java.util.ResourceBundle;

/**
 * Utility class for managing pages' URIs.
 * 
 * @author Anton Levchuk
 *
 */
public final class PageManager {
	
	private final static ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("resources.pages");

	private PageManager() {}
	
	/**
	 * Returns URI associated with the specified page's name.
	 * If the page is not present returns null.
	 * 
	 * @param pageName the page's name
	 * @param method the request's method
	 * @return the URI
	 */
	public static String getURI(String pageName, String method) {
		String URI = null;
		String key = method.toLowerCase() + "." + pageName;
		if (RESOURCE_BUNDLE.containsKey(key)) {
			URI = RESOURCE_BUNDLE.getString(key);
		}
		return URI;
	}
}
