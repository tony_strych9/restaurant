package com.levchuk.restaurant.util;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Utility class for passwords encoding.
 * 
 * @author Anton Levchuk
 *
 */
public final class PasswordEncoder {

	private PasswordEncoder() {}
	
	/**
	 * Encodes the given password.
	 * 
	 * @param password the password to be encoded
	 * @return the encoded password
	 */
	public static String encode(String password) {
		
		return DigestUtils.md5Hex(password);
		
	}
	
}
