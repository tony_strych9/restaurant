package com.levchuk.restaurant.util;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Utility class for managing and localizing application's messages.
 * 
 * @author Anton Levchuk
 *
 */
public final class MessageManager {
	
    private final static ResourceBundle RESOURCE_BUNDLE_RU = ResourceBundle.getBundle("resources.messages", new Locale("ru", "RU"));
    private final static ResourceBundle RESOURCE_BUNDLE_DEF = ResourceBundle.getBundle("resources.messages", new Locale("en", "US"));
	
    private MessageManager() {}
	
	/**
	 * Retrieves an appropriate message mapped by the specified key.
	 * 
	 * @param key the key
	 * @param language the language
	 * @param text the text to be inserted in the place of question marks
	 * @return the message
	 */
	public static String getMessage(String key, String language, String... text) {
		String message = null;
		if (language != null && "ru_RU".equals(language)) {
			if (RESOURCE_BUNDLE_RU.containsKey(key)) {
				message = RESOURCE_BUNDLE_RU.getString(key);
			}
		} else {
			if (RESOURCE_BUNDLE_DEF.containsKey(key)) {
				message = RESOURCE_BUNDLE_DEF.getString(key);
			}
		}
		if (message != null) {
			for (int i = 0; i < text.length; i++) {
				message = message.replaceFirst("\\?", text[i]);
			}
		} else {
			message = "???" + key + "???";
		}
		return message;
	}
	
}
