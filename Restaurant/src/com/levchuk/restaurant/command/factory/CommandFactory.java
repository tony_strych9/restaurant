package com.levchuk.restaurant.command.factory;

import javax.servlet.http.HttpServletRequest;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.command.util.CommandEnum;

/**
 * A factory class for creating command objects.
 * 
 * @author Anton Levchuk
 *
 */
public class CommandFactory {

	/**
	 * Returns the requested ActionCommand implementation.
	 * 
	 * @param request the request
	 * @return the command object
	 */
	public ActionCommand getCommand(HttpServletRequest request) {
		String action = request.getParameter("command");
		CommandEnum currentCommand = CommandEnum.commandValue(action);
		return currentCommand.getCommand();
	}
	
}
