package com.levchuk.restaurant.command.admin;

import javax.servlet.http.HttpServletRequest;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.util.PageManager;

/**
 * Encapsulates the operation of changing dishes' displaying criteria 
 * in the menu management section.
 * 
 * @author Anton Levchuk
 *
 */
public class ShowInactiveDishesCommand implements ActionCommand {
	
	private static final String MENU_MANAGEMENT_PAGE = "menuManagement";

	@Override
	public String execute(HttpServletRequest request) {
		String uri = PageManager.getURI(MENU_MANAGEMENT_PAGE, request.getMethod());
		request.getSession().setAttribute("showInactiveBox", request.getParameter("showInactiveBox"));
		return uri;
	}

}
