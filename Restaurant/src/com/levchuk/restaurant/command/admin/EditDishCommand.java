package com.levchuk.restaurant.command.admin;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.entity.Dish;
import com.levchuk.restaurant.entity.enums.DishStatus;
import com.levchuk.restaurant.service.MenuService;
import com.levchuk.restaurant.service.ServiceException;
import com.levchuk.restaurant.util.PageManager;
import com.levchuk.restaurant.util.MessageManager;

/**
 * Encapsulates the operation of a dish editing.
 * 
 * @author Anton Levchuk
 *
 */
public class EditDishCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(EditDishCommand.class);
	private static final String MENU_MANAGEMENT_PAGE = "menuManagement";
	private static final String ERROR_PAGE = "error500";

	@Override
	public String execute(HttpServletRequest request) {
		String uri = PageManager.getURI(MENU_MANAGEMENT_PAGE, request.getMethod());
		HttpSession session = request.getSession();
		String lang = (String) session.getAttribute("language");
		int dishID = 0;
		if (request.getParameter("dishID") != null && !"".equals(request.getParameter("dishID"))) {
			dishID = Integer.parseInt(request.getParameter("dishID"));
		}
		if (dishID != 0) {
			String name = request.getParameter("dishName");
			String price = request.getParameter("dishPrice");
			String description = request.getParameter("dishDescription");
			String status = request.getParameter("dishStatus");
			String image = null;
			Part imagePart = null;
			try {
				imagePart = request.getPart("imageFile");
				image = imagePart.getSubmittedFileName();
			} catch (IOException | ServletException e1) {
				LOG.error(e1);
			}
			try {
				Dish dish = MenuService.retrieveDish(dishID);
				Map<String, String> errors = 
						MenuService.validateEditingInfo(dish.getName(), name, price, description, image, lang);
				if (errors.isEmpty()) {
					dish.setName(name);
					dish.setPrice(Double.parseDouble(price));
					dish.setDescription(description);
					dish.setStatus(DishStatus.valueOf(status));
					if (image != null && !image.isEmpty()) {
						dish.setImage(MenuService.uploadImage(imagePart, name));
					}
					MenuService.updateDish(dish);
					session.setAttribute("dishChangedMessage", 
							MessageManager.getMessage("message.menu.changed", lang));
				} else {
					for (Entry<String, String> error : errors.entrySet()) {
						session.setAttribute(error.getKey(), error.getValue());
					}
					session.setAttribute("dishChangedError", 
							MessageManager.getMessage("error.menu.changed", lang));
				}
			} catch (ServiceException e) {
				LOG.error(MessageManager.getMessage(e.getMessage(), ""), e);
				session.setAttribute("errorPageMessage", MessageManager.getMessage(e.getMessage(), lang));
				uri = PageManager.getURI(ERROR_PAGE, request.getMethod());
			}
		}
		return uri;
	}

}
