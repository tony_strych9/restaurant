package com.levchuk.restaurant.command.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.service.OrderService;
import com.levchuk.restaurant.service.ServiceException;
import com.levchuk.restaurant.util.MessageManager;
import com.levchuk.restaurant.util.PageManager;

/**
 * Encapsulates the operation of navigating to the orders management section.
 * 
 * @author Anton Levchuk
 *
 */
public class ManageOrdersCommand implements ActionCommand{
	
	private static final Logger LOG = Logger.getLogger(ManageOrdersCommand.class);
	private static final String ORDERS_MANAGEMENT_PAGE = "ordersManagement";
	private static final String ERROR_PAGE = "error500";

	@Override
	public String execute(HttpServletRequest request) {
		String uri = PageManager.getURI(ORDERS_MANAGEMENT_PAGE, request.getMethod());
		HttpSession session = request.getSession();
		String lang = (String) session.getAttribute("language");
		try {
			request.setAttribute("orders", OrderService.retrieveAllCurrentOrders());
		} catch (ServiceException e) {
			LOG.error(MessageManager.getMessage(e.getMessage(), ""), e);
			session.setAttribute("errorPageMessage", MessageManager.getMessage(e.getMessage(), lang));
			uri = PageManager.getURI(ERROR_PAGE, request.getMethod());
		}
		return uri;
	}

}
