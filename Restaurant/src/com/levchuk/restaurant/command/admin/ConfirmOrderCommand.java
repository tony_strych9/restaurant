package com.levchuk.restaurant.command.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.entity.Item;
import com.levchuk.restaurant.entity.Order;
import com.levchuk.restaurant.entity.enums.DishStatus;
import com.levchuk.restaurant.entity.enums.OrderStatus;
import com.levchuk.restaurant.service.OrderService;
import com.levchuk.restaurant.service.ServiceException;
import com.levchuk.restaurant.util.PageManager;
import com.levchuk.restaurant.util.MessageManager;

/**
 * Encapsulates the operation of order confirmation.
 * 
 * @author Anton Levchuk
 *
 */
public class ConfirmOrderCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(ConfirmOrderCommand.class);
	private static final String ORDERS_MANAGEMENT_PAGE = "ordersManagement";
	private static final String ERROR_PAGE = "error500";

	@Override
	public String execute(HttpServletRequest request) {
		String uri = PageManager.getURI(ORDERS_MANAGEMENT_PAGE, request.getMethod());
		HttpSession session = request.getSession();
		String lang = (String) session.getAttribute("language");
		int orderID = 0;
		if (request.getParameter("orderID") != null) {
			orderID = Integer.parseInt(request.getParameter("orderID"));
			request.setAttribute("orderID", orderID);
		}
		if (orderID != 0) {
			try {
				Order order = OrderService.retrieveOrderByID(orderID);
				if (order != null) {
					switch (order.getStatus()) {
					case SUBMITTED:
						List<Item> items = OrderService.retrieveOrderItems(orderID);
						if (items.isEmpty()) {
							session.setAttribute("emptyError", 
									MessageManager.getMessage("error.order.empty", lang, String.valueOf(orderID)));
						} else {
							int quantity = (int) items.stream().
									filter(d -> d.getDishStatus().equals(DishStatus.INACTIVE)).count();
							if (quantity == 0) {
								OrderService.updateOrderStatus(orderID, OrderStatus.CONFIRMED);
								session.setAttribute("confirmedMessage", 
										MessageManager.getMessage("message.order.confirmed", lang, String.valueOf(orderID)));
							} else {
								session.setAttribute("inactiveError", 
										MessageManager.getMessage("error.order.inactive", lang, String.valueOf(orderID)));
							}
						}
						break;
					case CONFIRMED:
						session.setAttribute("unpaidError", 
								MessageManager.getMessage("message.order.unpaid", lang, String.valueOf(orderID)));	
						break;
					case CANCELED:
						session.setAttribute("canceledError", 
								MessageManager.getMessage("message.order.canceled", lang, String.valueOf(orderID)));
						break;
					default:
						break;
					}
				}
			} catch (ServiceException e) {
				LOG.error(MessageManager.getMessage(e.getMessage(), ""), e);
				session.setAttribute("errorPageMessage", MessageManager.getMessage(e.getMessage(), lang));
				uri = PageManager.getURI(ERROR_PAGE, request.getMethod());
			}
		}
		return uri;
	}

}
