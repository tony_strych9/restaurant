package com.levchuk.restaurant.command.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.entity.User;
import com.levchuk.restaurant.service.ServiceException;
import com.levchuk.restaurant.service.UserService;
import com.levchuk.restaurant.util.MessageManager;
import com.levchuk.restaurant.util.PageManager;

/**
 * Encapsulates the operation of navigating to the users management section.
 * 
 * @author Anton Levchuk
 *
 */
public class ManageUsersCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(ManageUsersCommand.class);
	private static final String ERROR_PAGE = "error500";
	private static final String USERS_MANAGEMENT_PAGE = "usersManagement";

	@Override
	public String execute(HttpServletRequest request) {
		String uri = PageManager.getURI(USERS_MANAGEMENT_PAGE, request.getMethod());
		HttpSession session = request.getSession();
		String lang = (String) session.getAttribute("language");
		try {
			int userID = 0;
			if (request.getParameter("userID") != null) {
				userID = Integer.parseInt(request.getParameter("userID"));
			} else if (session.getAttribute("userID") != null) {
				userID = (int) session.getAttribute("userID");
			} else if (request.getParameter("orderID") != null) {
				int orderID = Integer.parseInt(request.getParameter("orderID"));
				userID = ((User) UserService.retrieveUserByOrder(orderID)).getUserID();
			}
			List<User> users = UserService.retrieveUsers();
			request.setAttribute("users", users);
			if (!users.isEmpty()) {
				User user = UserService.retrieveUser(userID);
				if (user == null) {
					user = users.get(0);
					userID = user.getUserID();
				}
				request.setAttribute("userID", userID);
				request.setAttribute("user", user);
			}
		} catch (ServiceException e) {
			LOG.error(MessageManager.getMessage(e.getMessage(), ""), e);
			session.setAttribute("errorPageMessage", MessageManager.getMessage(e.getMessage(), lang));
			uri = PageManager.getURI(ERROR_PAGE, request.getMethod());
		}
		return uri;
	}

}
