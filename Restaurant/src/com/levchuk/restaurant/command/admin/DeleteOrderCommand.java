package com.levchuk.restaurant.command.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.service.OrderService;
import com.levchuk.restaurant.service.ServiceException;
import com.levchuk.restaurant.util.MessageManager;
import com.levchuk.restaurant.util.PageManager;

/**
 * Encapsulates the operation of deleting an order.
 * 
 * @author Anton Levchuk
 *
 */
public class DeleteOrderCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(DeleteOrderCommand.class);
	private static final String ORDERS_MANAGEMENT_PAGE = "ordersManagement";
	private static final String ERROR_PAGE = "error500";

	@Override
	public String execute(HttpServletRequest request) {
		HttpSession session = request.getSession();
		String lang = (String) session.getAttribute("language");
		String uri = PageManager.getURI(ORDERS_MANAGEMENT_PAGE, request.getMethod());
		int orderID = 0;
		if (request.getParameter("orderID") != null) {
			orderID = Integer.parseInt(request.getParameter("orderID"));
		}
		if (orderID != 0) {
			try {
				OrderService.deleteOrder(orderID);
				session.setAttribute("deletedMessage", 
						MessageManager.getMessage("message.order.deleted", lang, String.valueOf(orderID)));
			} catch (ServiceException e) {
				LOG.error(MessageManager.getMessage(e.getMessage(), ""), e);
				session.setAttribute("errorPageMessage", MessageManager.getMessage(e.getMessage(), lang));
				uri = PageManager.getURI(ERROR_PAGE, request.getMethod());
			}
		}
		return uri;
	}

}
