package com.levchuk.restaurant.command.admin;

import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.entity.User;
import com.levchuk.restaurant.entity.enums.UserRole;
import com.levchuk.restaurant.service.ServiceException;
import com.levchuk.restaurant.service.UserService;
import com.levchuk.restaurant.util.MessageManager;
import com.levchuk.restaurant.util.PageManager;
import com.levchuk.restaurant.util.PasswordEncoder;
import com.levchuk.restaurant.util.SessionManager;

/**
 * Encapsulates the operation of a user editing.
 * 
 * @author Anton Levchuk
 *
 */
public class EditUserCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(EditUserCommand.class);
	private static final String USERS_MANAGEMENT_PAGE = "usersManagement";
	private static final String ERROR_PAGE = "error500";

	@Override
	public String execute(HttpServletRequest request) {
		String uri = PageManager.getURI(USERS_MANAGEMENT_PAGE, request.getMethod());
		HttpSession session = request.getSession();
		String lang = (String) session.getAttribute("language");
		int userID = 0;
		if (request.getParameter("userID") != null) {
			userID = Integer.parseInt(request.getParameter("userID"));
		}
		if (userID != 0) {
			session.setAttribute("userID", userID);
			String login = request.getParameter("login");
			String password = request.getParameter("password");
			String repeatPassword = request.getParameter("repeatPassword");
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String email = request.getParameter("email");
			String role = request.getParameter("role");
			try {
				User user = UserService.retrieveUser(userID);
				String oldLogin = user.getLogin();
				Map<String, String> errors = UserService.validateInfoToEdit(oldLogin, login, password, 
						repeatPassword, firstName, lastName, user.getEmail(), email, lang);
				if (errors.isEmpty()) {
					user.setLogin(login);
					if (password != null && !password.equals("")) {
						user.setPassword(PasswordEncoder.encode(password));
					}
					user.setFirstName(firstName);
					user.setLastName(lastName);
					user.setEmail(email);
					if (role != null) {
						user.setRole(UserRole.valueOf(role));
					}
					UserService.updateUser(user);
					SessionManager.getSessionManager().removeSession(user.getUserID());
					session.setAttribute("editedMessage",
							MessageManager.getMessage("message.user.edited", lang, oldLogin));
				} else {
					for (Entry<String, String> error : errors.entrySet()) {
						session.setAttribute(error.getKey(), error.getValue());
					}
					session.setAttribute("editedError",
							MessageManager.getMessage("error.user.edited", lang));
				}
			} catch (ServiceException e) {
				LOG.error(MessageManager.getMessage(e.getMessage(), ""), e);
				session.setAttribute("errorPageMessage", MessageManager.getMessage(e.getMessage(), lang));
				uri = PageManager.getURI(ERROR_PAGE, request.getMethod());
			}
		}
		return uri;
	}

}
