package com.levchuk.restaurant.command.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.entity.Dish;
import com.levchuk.restaurant.entity.MenuSection;
import com.levchuk.restaurant.entity.enums.OrderStatus;
import com.levchuk.restaurant.service.MenuService;
import com.levchuk.restaurant.service.OrderService;
import com.levchuk.restaurant.service.ServiceException;
import com.levchuk.restaurant.util.MessageManager;
import com.levchuk.restaurant.util.PageManager;

/**
 * Encapsulates the operation of navigating to the order editor section.
 * 
 * @author Anton Levchuk
 *
 */
public class ShowOrderEditorCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(ShowOrderEditorCommand.class);
	private static final String ORDER_EDITOR_PAGE = "orderEditor";
	private static final String ORDERS_MANAGEMENT_PAGE = "ordersManagement";
	private static final String ERROR_PAGE = "error500";

	@Override
	public String execute(HttpServletRequest request) {
		String uri = null;
		HttpSession session = request.getSession();
		String lang = (String) session.getAttribute("language");
		int orderID = 0;
		if (request.getParameter("orderID") != null && !request.getParameter("orderID").isEmpty()) {
			orderID = Integer.parseInt(request.getParameter("orderID"));
			session.setAttribute("orderID", orderID);
		} else if (session.getAttribute("orderID") != null) {
			orderID = (int) session.getAttribute("orderID");
		}
		try {
			if (orderID != 0) {
				request.setAttribute("items", OrderService.retrieveOrderItems(orderID));
				List<MenuSection> sections = MenuService.retrieveSections();
				List<Dish> dishes = null;
				if (!sections.isEmpty()) {
					int sectionID = 0;
					if (session.getAttribute("sectionID") != null) {
						sectionID = (int) session.getAttribute("sectionID");
					} else {
						sectionID = sections.get(0).getSectionID();
					}
					request.setAttribute("sectionID", sectionID);
					request.setAttribute("sections", sections);
					dishes = MenuService.retrieveActiveDishes(sectionID);
					request.setAttribute("dishes", dishes);
					if (!dishes.isEmpty()) {
						int dishID = 0;
						if (session.getAttribute("dishID") != null) {
							dishID = (int) session.getAttribute("dishID");
						} else {
							dishID = dishes.get(0).getDishID();
						}
						request.setAttribute("dishID", dishID);
						request.setAttribute("dish", MenuService.retrieveDish(dishID));
					}
				}
				uri = PageManager.getURI(ORDER_EDITOR_PAGE, request.getMethod());
			} else {
				request.setAttribute("orders", OrderService.retrieveOrdersByStatus(OrderStatus.SUBMITTED));
				uri = PageManager.getURI(ORDERS_MANAGEMENT_PAGE, request.getMethod());
			}
		} catch (ServiceException e) {
			LOG.error(MessageManager.getMessage(e.getMessage(), ""), e);
			session.setAttribute("errorPageMessage", MessageManager.getMessage(e.getMessage(), lang));
			uri = PageManager.getURI(ERROR_PAGE, request.getMethod());
		}
		return uri;
	}

}
