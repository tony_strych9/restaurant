package com.levchuk.restaurant.command.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.entity.Dish;
import com.levchuk.restaurant.entity.MenuSection;
import com.levchuk.restaurant.service.MenuService;
import com.levchuk.restaurant.service.ServiceException;
import com.levchuk.restaurant.util.MessageManager;
import com.levchuk.restaurant.util.PageManager;

/**
 * Encapsulates the operation of navigating to the menu management section.
 * 
 * @author Anton Levchuk
 *
 */
public class ManageMenuCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(ManageMenuCommand.class);
	private static final String MENU_MANAGEMENT_PAGE = "menuManagement";
	private static final String ERROR_PAGE = "error500";

	@Override
	public String execute(HttpServletRequest request) {
		String uri = PageManager.getURI(MENU_MANAGEMENT_PAGE, request.getMethod());
		HttpSession session = request.getSession();
		String lang = (String) session.getAttribute("language");
		try {
			List<MenuSection> sections = MenuService.retrieveSections();
			request.setAttribute("sections", sections);
			if (!sections.isEmpty()) {
				int sectionID = 0;
				if (session.getAttribute("sectionID") != null) {
					sectionID = (int) session.getAttribute("sectionID");
				} else {
					sectionID = sections.get(0).getSectionID();
				}
				request.setAttribute("sectionID", sectionID);
				request.setAttribute("sections", sections);
				List<Dish> dishes = null;
				if (session.getAttribute("showInactiveBox") == null) {
					dishes = MenuService.retrieveActiveDishes(sectionID);
				} else {
					dishes = MenuService.retrieveDishes(sectionID);
				}
				request.setAttribute("dishes", dishes);
				if (dishes != null && !dishes.isEmpty()) {
					int dishID = 0;
					if (session.getAttribute("dishID") != null) {
						dishID = (int) session.getAttribute("dishID");
					} else {
						dishID = dishes.get(0).getDishID();
					}
					request.setAttribute("dishID", dishID);
					request.setAttribute("dish", MenuService.retrieveDish(dishID));
				}
			}
		} catch (ServiceException e) {
			LOG.error(MessageManager.getMessage(e.getMessage(), ""), e);
			session.setAttribute("errorPageMessage", MessageManager.getMessage(e.getMessage(), lang));
			uri = PageManager.getURI(ERROR_PAGE, request.getMethod());
		}
		return uri;
	}

}
