package com.levchuk.restaurant.command.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.util.MessageManager;
import com.levchuk.restaurant.util.PageManager;

/**
 * Represents an unrecognized command.
 * 
 * @author Anton Levchuk
 */
public class UndefinedCommand implements ActionCommand {
	
	private static final String ERROR_PAGE = "error500";
	
    public String execute(HttpServletRequest request) {
    	String uri = PageManager.getURI(ERROR_PAGE, request.getMethod());
    	HttpSession session = request.getSession();
    	String language = (String) session.getAttribute("language");
    	request.getSession().setAttribute("errorPageMessage", 
    			MessageManager.getMessage("error.common.undefinedCommand", language));
        return uri;
    }
}
