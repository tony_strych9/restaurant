package com.levchuk.restaurant.command.common;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.entity.Dish;
import com.levchuk.restaurant.service.MenuService;
import com.levchuk.restaurant.service.OrderService;
import com.levchuk.restaurant.service.ServiceException;
import com.levchuk.restaurant.util.MessageManager;
import com.levchuk.restaurant.util.PageManager;

/**
 * Encapsulates the operation of displaying dish's information.
 * 
 * @author Anton Levchuk
 *
 */
public class ShowDishInfoCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(ShowDishInfoCommand.class);
	private static final String MENU_PAGE = "menu";
	private static final String ORDER_FORM_PAGE = "orderForm";
	private static final String ORDER_EDITOR_PAGE = "orderEditor";
	private static final String MENU_MANAGEMENT_PAGE = "menuManagement";
	private static final String ERROR_PAGE = "error500";

	@Override
	public String execute(HttpServletRequest request) {
		String pageName = request.getParameter("pageName");
		String uri = PageManager.getURI(pageName, request.getMethod());
		HttpSession session = request.getSession();
		String lang = (String) session.getAttribute("language");
		int sectionID = 0;
		int dishID = 0;
		int orderID = 0;
		if (request.getParameter("sectionID") != null) {
			sectionID = Integer.parseInt(request.getParameter("sectionID"));
		}
		if (request.getParameter("dishID") != null) {
			dishID = Integer.parseInt(request.getParameter("dishID"));
			session.setAttribute("dishID", dishID);
		}
		if (session.getAttribute("orderID") != null) {
			orderID = (int) session.getAttribute("orderID");
		}
		request.setAttribute("sectionID", sectionID);
		request.setAttribute("dishID", dishID);
		try {
			request.setAttribute("sections", MenuService.retrieveSections());
			request.setAttribute("dish", MenuService.retrieveDish(dishID));
			List<Dish> dishes = null;
			switch (Optional.ofNullable(pageName).orElse("")) {
			case MENU_PAGE:
				dishes = MenuService.retrieveActiveDishes(sectionID);
				break;
			case ORDER_FORM_PAGE:
				dishes = MenuService.retrieveActiveDishes(sectionID);
				request.setAttribute("items", OrderService.retrieveOrderItems(orderID));
				break;
			case ORDER_EDITOR_PAGE:
				dishes = MenuService.retrieveActiveDishes(sectionID);
				request.setAttribute("items", OrderService.retrieveOrderItems(orderID));
				break;
			case MENU_MANAGEMENT_PAGE:
				if (session.getAttribute("showInactiveBox") == null) {
					dishes = MenuService.retrieveActiveDishes(sectionID);
				} else {
					dishes = MenuService.retrieveDishes(sectionID);
				}
				break;
			default:
				break;
			}
			request.setAttribute("dishes", dishes);
		} catch (ServiceException e) {
			LOG.error(MessageManager.getMessage(e.getMessage(), ""), e);
			session.setAttribute("errorPageMessage", MessageManager.getMessage(e.getMessage(), lang));
			uri = PageManager.getURI(ERROR_PAGE, request.getMethod());
		}
		return uri;
	}

	
	
}
