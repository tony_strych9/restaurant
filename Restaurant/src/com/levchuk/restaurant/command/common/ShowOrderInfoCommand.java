package com.levchuk.restaurant.command.common;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.entity.User;
import com.levchuk.restaurant.entity.enums.OrderStatus;
import com.levchuk.restaurant.service.OrderService;
import com.levchuk.restaurant.service.ServiceException;
import com.levchuk.restaurant.util.MessageManager;
import com.levchuk.restaurant.util.PageManager;


/**
 * Encapsulates the operation of examining order's information .
 * 
 * @author Anton Levchuk
 *
 */
public class ShowOrderInfoCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(ShowOrderInfoCommand.class);
	private static final String ORDERS_MANAGEMENT_PAGE = "ordersManagement";
	private static final String CURRENT_ORDERS_PAGE = "currentOrders";
	private static final String ORDERS_HISTORY_PAGE = "ordersHistory";
	private static final String ERROR_PAGE = "error500";

	@Override
	public String execute(HttpServletRequest request) {
		String pageName = request.getParameter("pageName");
		String uri = PageManager.getURI(pageName, request.getMethod());
		HttpSession session = request.getSession();
		String lang = (String) session.getAttribute("language");
		User user = (User) session.getAttribute("user");
		int orderID = 0;
		if (request.getParameter("orderID") != null && !request.getParameter("orderID").isEmpty()) {
			orderID = Integer.parseInt(request.getParameter("orderID"));
			request.setAttribute("orderID", orderID);
		}
		try {
			switch (Optional.ofNullable(pageName).orElse("")) {
			case ORDERS_MANAGEMENT_PAGE:
				if (orderID != 0) {
					request.setAttribute("orderInfo", OrderService.buildInfoByAdmin(orderID, lang));
				}
				request.setAttribute("orders", OrderService.retrieveAllCurrentOrders());
				request.setAttribute("status", OrderService.retrieveOrderByID(orderID).getStatus());
				break;
			case CURRENT_ORDERS_PAGE:
				if (orderID != 0) {
					request.setAttribute("orderInfo", OrderService.buildInfoByClient(orderID, lang));
				}
				request.setAttribute("orders", OrderService.retrieveCurrentOrders(user.getUserID()));
				request.setAttribute("status", OrderService.retrieveOrderByID(orderID).getStatus());
				break;
			case ORDERS_HISTORY_PAGE:
				if (orderID != 0) {
					request.setAttribute("orderInfo", OrderService.buildInfoByClient(orderID, lang));
				}
				request.setAttribute("orders", 
						OrderService.retrieveOrdersByUserAndStatus(user.getUserID(), OrderStatus.PAID));
				break;
			default:
				break;
			}
		} catch (ServiceException e) {
			LOG.error(MessageManager.getMessage(e.getMessage(), ""), e);
			session.setAttribute("errorPageMessage", MessageManager.getMessage(e.getMessage(), lang));
			uri = PageManager.getURI(ERROR_PAGE, request.getMethod());
		}
		return uri;
	}

}
