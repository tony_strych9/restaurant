package com.levchuk.restaurant.command.common;

import javax.servlet.http.HttpServletRequest;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.util.PageManager;

/**
 * Encapsulates the operation of changing the language.
 * 
 * @author Anton Levchuk
 */
public class ChangeLanguageCommand implements ActionCommand {
	
	@Override
	public String execute(HttpServletRequest request) {
		String pageName = request.getParameter("pageName");
		String uri = PageManager.getURI(pageName, request.getMethod());
		String language = request.getParameter("language");
		request.getSession().setAttribute("language", language);
		return uri;
	}

}
