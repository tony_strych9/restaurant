package com.levchuk.restaurant.command.common;

import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.entity.User;
import com.levchuk.restaurant.entity.enums.UserRole;
import com.levchuk.restaurant.service.ServiceException;
import com.levchuk.restaurant.service.UserService;
import com.levchuk.restaurant.util.PageManager;
import com.levchuk.restaurant.util.PasswordEncoder;
import com.levchuk.restaurant.util.MessageManager;

/**
 * Encapsulates the operation of creating new user's account.
 * 
 * @author Anton Levchuk
 */
public class SignupCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(SignupCommand.class);
	private static final String SIGNUP_PAGE = "signup";
	private static final String ERROR_PAGE = "error500";

	@Override
	public String execute(HttpServletRequest request) {
		String uri = PageManager.getURI(SIGNUP_PAGE, request.getMethod());
		HttpSession session = request.getSession();
		String lang = (String) session.getAttribute("language");
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		String repeatPassword = request.getParameter("repeatPassword");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");
		String role = request.getParameter("role");
		try {
			Map<String, String> errors = UserService.validateInfoToSignup(login, password, repeatPassword, 
					firstName, lastName, email, lang);
			if (errors.isEmpty()) {
				User user = new User();
				user.setLogin(login);
				user.setPassword(PasswordEncoder.encode(password));
				user.setFirstName(firstName);
				user.setLastName(lastName);
				user.setEmail(email);
				if (role == null) {
					user.setRole(UserRole.CLIENT);
				} else {
					user.setRole(UserRole.valueOf(role));
				}
				user.setLastVisit(new Date());
				UserService.createUser(user);
				session.setAttribute("signupMessage",
						MessageManager.getMessage("message.user.signup", lang, login));
			} else {
				for (Entry<String, String> error : errors.entrySet()) {
					session.setAttribute(error.getKey(), error.getValue());
				}
				session.setAttribute("signupError",
						MessageManager.getMessage("error.user.signup", lang));
			}
		} catch (ServiceException e) {
			LOG.error(MessageManager.getMessage(e.getMessage(), ""), e);
			session.setAttribute("errorPageMessage", MessageManager.getMessage(e.getMessage(), lang));
			uri = PageManager.getURI(ERROR_PAGE, request.getMethod());
		}
		return uri;
	}

}
