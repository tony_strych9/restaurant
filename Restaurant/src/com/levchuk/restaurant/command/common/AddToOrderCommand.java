package com.levchuk.restaurant.command.common;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.entity.Order;
import com.levchuk.restaurant.entity.User;
import com.levchuk.restaurant.service.MenuService;
import com.levchuk.restaurant.service.OrderService;
import com.levchuk.restaurant.service.ServiceException;
import com.levchuk.restaurant.util.MessageManager;
import com.levchuk.restaurant.util.PageManager;

/**
 * Encapsulates the operation of adding a dish to the order.
 * 
 * @author Anton Levchuk
 * 
 */
public class AddToOrderCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(AddToOrderCommand.class);
	private static final String ERROR_PAGE = "error500";
	
	@Override
	public String execute(HttpServletRequest request) {
		String pageName = request.getParameter("pageName");
		String uri = PageManager.getURI(pageName, request.getMethod());
		String lang = (String) request.getSession().getAttribute("language");
		HttpSession session = request.getSession();
		int orderID = 0;
		int dishID = 0;
		String dishName = null;
		try {
			if (session.getAttribute("orderID") != null) {
				orderID = (int) session.getAttribute("orderID");
			} else {
				User user = (User) session.getAttribute("user");
				Order order = OrderService.retrieveFormingOrder(user.getUserID());
				if (order != null) {
					orderID = order.getOrderID();
				}
			}
			if (request.getParameter("dishID") != null) {
				dishID = Integer.parseInt(request.getParameter("dishID"));
				dishName = MenuService.retrieveDish(dishID).getName();
				OrderService.addDishToOrder(orderID, dishID);
				session.setAttribute("dishAddedMessage", 
						MessageManager.getMessage("message.order.dishAdded", lang, dishName));
			}
		} catch (ServiceException e) {
			LOG.error(MessageManager.getMessage(e.getMessage(), ""), e);
			session.setAttribute("errorPageMessage", MessageManager.getMessage(e.getMessage(), lang));
			uri = PageManager.getURI(ERROR_PAGE, request.getMethod());
		}
		return uri;
	}

}
