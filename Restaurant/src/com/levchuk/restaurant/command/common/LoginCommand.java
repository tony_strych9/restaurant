package com.levchuk.restaurant.command.common;


import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.entity.User;
import com.levchuk.restaurant.service.ServiceException;
import com.levchuk.restaurant.service.UserService;
import com.levchuk.restaurant.util.PageManager;
import com.levchuk.restaurant.util.SessionManager;
import com.levchuk.restaurant.util.MessageManager;

/**
 * Encapsulates the authorization operation.
 * 
 * @author Anton Levchuk
 */
public class LoginCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(LoginCommand.class);
	private static final String MAIN_PAGE = "main";
	private static final String LOGIN_PAGE = "login";
	private static final String ERROR_PAGE = "error500";
	
	public String execute(HttpServletRequest request) {
		String uri = null;
		HttpSession session = request.getSession();
		String lang = (String) session.getAttribute("language");
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		try {
			User user = UserService.retrieveUser(login, password);
			if (user != null) {
				user.setLastVisit(new Date());
				UserService.updateUser(user);
				session.setAttribute("user", user);
				SessionManager.getSessionManager().addSession(user.getUserID(), session.getId());
				session.setAttribute("loginMessage",
						MessageManager.getMessage("message.user.login", lang, login));
				uri = PageManager.getURI(MAIN_PAGE, request.getMethod());
			} else {
				session.setAttribute("loginError",
						MessageManager.getMessage("error.user.login", lang));
				uri = PageManager.getURI(LOGIN_PAGE, request.getMethod());
			}
		} catch (ServiceException e) {
			LOG.error(MessageManager.getMessage(e.getMessage(), ""), e);
			session.setAttribute("errorPageMessage", MessageManager.getMessage(e.getMessage(), lang));
			uri = PageManager.getURI(ERROR_PAGE, request.getMethod());;
		}
		return uri;
	}
}