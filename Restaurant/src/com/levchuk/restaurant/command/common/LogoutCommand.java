package com.levchuk.restaurant.command.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.entity.User;
import com.levchuk.restaurant.util.PageManager;
import com.levchuk.restaurant.util.SessionManager;

/**
 *  Encapsulates the deauthorization operation.
 *  
 *  @author Anton Levchuk
 */
public class LogoutCommand implements ActionCommand {

	private static final String INDEX_PAGE = "index";
	
	public String execute(HttpServletRequest request) {
		String uri = PageManager.getURI(INDEX_PAGE, request.getMethod());
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		SessionManager.getSessionManager().removeSession(user.getUserID());
		session.invalidate();
		return uri;
	}
}
