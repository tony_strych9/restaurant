package com.levchuk.restaurant.command.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.entity.Item;
import com.levchuk.restaurant.service.OrderService;
import com.levchuk.restaurant.service.ServiceException;
import com.levchuk.restaurant.util.MessageManager;
import com.levchuk.restaurant.util.PageManager;

/**
 * Encapsulates the operation of deleting an item from the order.
 * 
 * @author Anton Levchuk
 */
public class RemoveFromOrderCommand implements ActionCommand {

	private static final Logger LOG = Logger.getLogger(RemoveFromOrderCommand.class);
	private static final String ERROR_PAGE = "error500";
	
	@Override
	public String execute(HttpServletRequest request) {
		String pageName = request.getParameter("pageName");
		String uri = PageManager.getURI(pageName, request.getMethod());
		HttpSession session = request.getSession();
		String lang = (String) session.getAttribute("language");
		int itemID = 0;
		if (request.getParameter("itemID") != null) {
			itemID = Integer.parseInt(request.getParameter("itemID"));
		}
		if (itemID != 0) {
			try {
				Item item = OrderService.retrieveItem(itemID);
				if (item != null) {
					OrderService.removeDishFromOrder(item.getOrderID(), item.getDishID());
					request.setAttribute("itemID", itemID);
					session.setAttribute("deletedMessage", 
							MessageManager.getMessage("message.order.dishDeleted", lang, item.getDishName()));
				}
			} catch (ServiceException e) {
				LOG.error(MessageManager.getMessage(e.getMessage(), ""), e);
				session.setAttribute("errorPageMessage", MessageManager.getMessage(e.getMessage(), lang));
				uri = PageManager.getURI(ERROR_PAGE, request.getMethod());
			}
		}
		return uri;
	}
	
}
