package com.levchuk.restaurant.command.common;


import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.entity.Dish;
import com.levchuk.restaurant.service.MenuService;
import com.levchuk.restaurant.service.OrderService;
import com.levchuk.restaurant.service.ServiceException;
import com.levchuk.restaurant.util.MessageManager;
import com.levchuk.restaurant.util.PageManager;

/**
 * Encapsulates the operation of selecting menu's section.
 * 
 * @author Anton Levchuk
 */
public class SelectSectionCommand implements ActionCommand {

	private static final Logger LOG = Logger.getLogger(SelectSectionCommand.class);
	private static final String MENU_PAGE = "menu";
	private static final String ORDER_FORM_PAGE = "orderForm";
	private static final String ORDER_EDITOR_PAGE = "orderEditor";
	private static final String MENU_MANAGEMENT_PAGE = "menuManagement";
	private static final String ERROR_PAGE = "error500";
	
	@Override
	public String execute(HttpServletRequest request) {
		String pageName = request.getParameter("pageName");
		String uri = PageManager.getURI(pageName, request.getMethod());
		HttpSession session = request.getSession();
		String lang = (String) session.getAttribute("language");
		int orderID = 0;
		int sectionID = 0;
		if (session.getAttribute("orderID") != null) {
			orderID = (int) session.getAttribute("orderID");
		}
		if (request.getParameter("sectionID") != null) {
			sectionID = Integer.parseInt(request.getParameter("sectionID"));
			session.setAttribute("sectionID", sectionID);
		}
		request.setAttribute("sectionID", sectionID);
		try {
			request.setAttribute("sections", MenuService.retrieveSections());
			List<Dish> dishes = null;
			switch (Optional.ofNullable(pageName).orElse("")) {
			case MENU_PAGE:
				dishes = MenuService.retrieveActiveDishes(sectionID);
				break;
			case ORDER_FORM_PAGE:
				request.setAttribute("items", OrderService.retrieveOrderItems(orderID));
				dishes = MenuService.retrieveActiveDishes(sectionID);
				break;
			case ORDER_EDITOR_PAGE:
				request.setAttribute("items", OrderService.retrieveOrderItems(orderID));
				dishes = MenuService.retrieveActiveDishes(sectionID);
				break;
			case MENU_MANAGEMENT_PAGE:
				if (session.getAttribute("showInactiveBox") == null) {
					dishes = MenuService.retrieveActiveDishes(sectionID);
				} else {
					dishes = MenuService.retrieveDishes(sectionID);
				}
				break;
			default:
				break;
			}
			request.setAttribute("dishes", dishes);
			if (dishes != null && !dishes.isEmpty()) {
				int dishID = dishes.get(0).getDishID();
				request.setAttribute("dish", dishes.get(0));
				session.setAttribute("dishID", dishID);
				request.setAttribute("dishID", dishID);
			}
		} catch (ServiceException e) {
			LOG.error(MessageManager.getMessage(e.getMessage(), ""), e);
			session.setAttribute("errorPageMessage", MessageManager.getMessage(e.getMessage(), lang));
			uri = PageManager.getURI(ERROR_PAGE, request.getMethod());
		}
		return uri;
	}
	
}
