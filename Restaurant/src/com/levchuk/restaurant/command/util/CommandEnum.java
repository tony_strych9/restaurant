package com.levchuk.restaurant.command.util;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.command.admin.ConfirmOrderCommand;
import com.levchuk.restaurant.command.admin.CreateDishCommand;
import com.levchuk.restaurant.command.admin.DeleteOrderCommand;
import com.levchuk.restaurant.command.admin.ShowOrderEditorCommand;
import com.levchuk.restaurant.command.admin.ManageMenuCommand;
import com.levchuk.restaurant.command.admin.ManageOrdersCommand;
import com.levchuk.restaurant.command.admin.ManageUsersCommand;
import com.levchuk.restaurant.command.admin.ShowInactiveDishesCommand;
import com.levchuk.restaurant.command.admin.EditDishCommand;
import com.levchuk.restaurant.command.client.CancelOrderCommand;
import com.levchuk.restaurant.command.client.ClearOrderCommand;
import com.levchuk.restaurant.command.admin.EditUserCommand;
import com.levchuk.restaurant.command.client.EditProfileCommand;
import com.levchuk.restaurant.command.client.FormOrderCommand;
import com.levchuk.restaurant.command.client.PayOrderCommand;
import com.levchuk.restaurant.command.client.ShowCurrentOrdersCommand;
import com.levchuk.restaurant.command.client.ShowOrdersHistoryCommand;
import com.levchuk.restaurant.command.client.SubmitOrderCommand;
import com.levchuk.restaurant.command.common.AddToOrderCommand;
import com.levchuk.restaurant.command.common.ChangeLanguageCommand;
import com.levchuk.restaurant.command.common.LoginCommand;
import com.levchuk.restaurant.command.common.LogoutCommand;
import com.levchuk.restaurant.command.common.RemoveFromOrderCommand;
import com.levchuk.restaurant.command.common.SelectSectionCommand;
import com.levchuk.restaurant.command.common.ShowDishInfoCommand;
import com.levchuk.restaurant.command.common.ShowMenuCommand;
import com.levchuk.restaurant.command.common.ShowOrderInfoCommand;
import com.levchuk.restaurant.command.common.SignupCommand;
import com.levchuk.restaurant.command.common.UndefinedCommand;

/**
 * Stores all ActionCommand implementations.
 * 
 * @author Anton Levchuk
 */
public enum CommandEnum {
	
	UNDEFINED_COMMAND {
		{
			this.command = new UndefinedCommand();
		}
	},
	
	SHOW_MENU {
		{
			this.command = new ShowMenuCommand();
		}
	},
	
	SELECT_SECTION {
		{
			this.command = new SelectSectionCommand();
		}
	},
	
	LOGIN {
		{
			this.command = new LoginCommand();
		}
	},
	
	LOGOUT {
		{
			this.command = new LogoutCommand();
		}
	},
	
	SIGNUP {
		{
			this.command = new SignupCommand();
		}
	},
	
	ADD_TO_ORDER {
		{
			this.command = new AddToOrderCommand();
		}
	},
	
	FORM_ORDER {
		{
			this.command = new FormOrderCommand();
		}
		
	},
	
	CLEAR_ORDER {
		{
			this.command = new ClearOrderCommand();
		}
	},
	
	SUBMIT_ORDER {
		{
			this.command = new SubmitOrderCommand();
		}
	},
	
	REMOVE_FROM_ORDER {
		{
			this.command = new RemoveFromOrderCommand();
		}
	},
	
	MANAGE_ORDERS {
		{
			this.command = new ManageOrdersCommand();
		}
	},
	
	MANAGE_USERS {
		{
			this.command = new ManageUsersCommand();
		}
	},
	
	DELETE_ORDER {
		{
			this.command = new DeleteOrderCommand();
		}
	},
	
	CONFIRM_ORDER {
		{
			this.command = new ConfirmOrderCommand();
		}
	},
	
	SHOW_ORDER_INFO {
		{
			this.command = new ShowOrderInfoCommand();
		}
	},
	
	MANAGE_MENU {
		{
			this.command = new ManageMenuCommand();
		}
	},
	
	ORDER_EDITOR {
		{
			this.command = new ShowOrderEditorCommand();
		}
	},
	
	CURRENT_ORDERS {
		{
			this.command = new ShowCurrentOrdersCommand();
		}
	},
	
	ORDERS_HISTORY {
		{
			this.command = new ShowOrdersHistoryCommand();
		}
	}, 
	
	PAY_ORDER {
		{
			this.command = new PayOrderCommand();
		}
	},
	
	CANCEL_ORDER {
		{
			this.command = new CancelOrderCommand();
		}
	},
	
	EDIT_DISH {
		{
			this.command = new EditDishCommand();
		}
	},
	
	CHANGE_LANG {
		{
			this.command = new ChangeLanguageCommand();
		}
	},
	
	CREATE_DISH {
		{
			this.command = new CreateDishCommand();
		}
	},
	
	SHOW_DISH_INFO {
		{
			this.command = new ShowDishInfoCommand();
		}
	},
	
	SHOW_INACTIVE_DISHES {
		{
			this.command = new ShowInactiveDishesCommand();
		}
	},
	
	EDIT_USER {
		{
			this.command = new EditUserCommand();
		}
	},
	
	EDIT_PROFILE {
		{
			this.command = new EditProfileCommand();
		}
	};
	
	ActionCommand command;
	
	public ActionCommand getCommand() {
		return command;
	}
	
	/**
	 * Checks if the specified command is present.
	 * 
	 * @param commandName command's name
	 * @return false if the command is not present
	 */
	public static boolean contains(String commandName) {
		boolean result = false;
		if (commandName == null) {
			result = false;
		} else {
			for (CommandEnum command : values()) {
				if (command.name().equals(commandName.toUpperCase().replaceAll("\\s", ""))) {
					return true;
				}
			}
		}
		return result;
	}
	
	/**
	 * Returns CommandEnum instance representing the specified command.
	 * If the command hasn't been recognized returns UNDEFINED_COMMAND.
	 * 
	 * @param commandName command's name
	 * @return CommandEnum instance
	 */
	public static CommandEnum commandValue(String commandName) {
		CommandEnum command = UNDEFINED_COMMAND;
		if (commandName != null) {
			if (contains(commandName)) {
				command = valueOf(commandName.trim().toUpperCase().replaceAll("\\s", "_"));
			}
		}
		return command;
	}
	
}
