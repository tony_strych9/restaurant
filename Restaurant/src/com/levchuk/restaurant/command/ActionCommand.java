package com.levchuk.restaurant.command;

import javax.servlet.http.HttpServletRequest;

/**
 * Base interface to be implemented by all command classes.
 */
public interface ActionCommand {
	
	/**
	 * Processes encapsulated user's action.
	 * 
	 * @param request the received request
	 * @return the URI
	 */
	String execute(HttpServletRequest request);
	
}
