package com.levchuk.restaurant.command.client;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.entity.Order;
import com.levchuk.restaurant.entity.User;
import com.levchuk.restaurant.service.OrderService;
import com.levchuk.restaurant.service.ServiceException;
import com.levchuk.restaurant.util.MessageManager;
import com.levchuk.restaurant.util.PageManager;

/**
 * Encapsulates the operation of navigation to the current orders section.
 * 
 * @author Anton Levchuk
 */
public class ShowCurrentOrdersCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(ShowCurrentOrdersCommand.class);
	private static final String CURRENT_ORDERS_PAGE = "currentOrders";
	private static final String ERROR_PAGE = "error500";

	@Override
	public String execute(HttpServletRequest request) {
		String uri = PageManager.getURI(CURRENT_ORDERS_PAGE, request.getMethod());
		HttpSession session = request.getSession();
		String lang = (String) session.getAttribute("language");
		User user = (User) session.getAttribute("user");
		try {
			List<Order> currentOrders = OrderService.retrieveCurrentOrders(user.getUserID());
			request.setAttribute("orders", currentOrders);
		} catch (ServiceException e) {
			LOG.error(MessageManager.getMessage(e.getMessage(), ""), e);
			session.setAttribute("errorPageMessage", MessageManager.getMessage(e.getMessage(), lang));
			uri = PageManager.getURI(ERROR_PAGE, request.getMethod());
		}
		return uri;
	}

}
