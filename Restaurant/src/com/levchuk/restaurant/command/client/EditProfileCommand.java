package com.levchuk.restaurant.command.client;

import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.entity.User;
import com.levchuk.restaurant.entity.enums.UserRole;
import com.levchuk.restaurant.service.ServiceException;
import com.levchuk.restaurant.service.UserService;
import com.levchuk.restaurant.util.MessageManager;
import com.levchuk.restaurant.util.PageManager;
import com.levchuk.restaurant.util.PasswordEncoder;

/**
 * Encapsulates the operation of editing user's profile.
 * 
 * @author Anton Levchuk
 * 
 */
public class EditProfileCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(EditProfileCommand.class);
	private static final String PROFILE_EDITOR_PAGE = "profileEditor";
	private static final String ERROR_PAGE = "error500";

	@Override
	public String execute(HttpServletRequest request) {
		String uri = PageManager.getURI(PROFILE_EDITOR_PAGE, request.getMethod());
		HttpSession session = request.getSession();
		String lang = (String) session.getAttribute("language");
		String password = request.getParameter("password");
		String repeatPassword = request.getParameter("repeatPassword");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");
		String role = request.getParameter("role");
		User user = (User) session.getAttribute("user");
		try {
			Map<String, String> errors = UserService.validateInfoToEdit(user.getLogin(), user.getLogin(), 
					password, repeatPassword, firstName, lastName, user.getEmail(), email, lang);
			if (errors.isEmpty()) {
				if (password != null && !password.equals("")) {
					user.setPassword(PasswordEncoder.encode(password));
				}
				user.setFirstName(firstName);
				user.setLastName(lastName);
				user.setEmail(email);
				if (role != null) {
					user.setRole(UserRole.valueOf(role));
				}
				UserService.updateUser(user);
				session.setAttribute("editedMessage",
						MessageManager.getMessage("message.user.changed", lang));
			} else {
				for (Entry<String, String> error : errors.entrySet()) {
					session.setAttribute(error.getKey(), error.getValue());
				}
				session.setAttribute("editedError",
						MessageManager.getMessage("error.user.changed", lang));
			}
		} catch (ServiceException e) {
			LOG.error(MessageManager.getMessage(e.getMessage(), ""), e);
			session.setAttribute("errorPageMessage", MessageManager.getMessage(e.getMessage(), lang));
			uri = PageManager.getURI(ERROR_PAGE, request.getMethod());
		}
		
		return uri;
	}

}
