package com.levchuk.restaurant.command.client;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.entity.Dish;
import com.levchuk.restaurant.entity.MenuSection;
import com.levchuk.restaurant.entity.Order;
import com.levchuk.restaurant.entity.User;
import com.levchuk.restaurant.service.MenuService;
import com.levchuk.restaurant.service.OrderService;
import com.levchuk.restaurant.service.ServiceException;
import com.levchuk.restaurant.util.MessageManager;
import com.levchuk.restaurant.util.PageManager;

/**
 * Encapsulates the operation of navigation to the order forming section.
 * 
 * @author Anton Levchuk
 */
public class FormOrderCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(FormOrderCommand.class);
	private static final String ORDER_FORM_PAGE = "orderForm";
	private static final String ERROR_PAGE = "error500";

	@Override
	public String execute(HttpServletRequest request) {
		String uri = PageManager.getURI(ORDER_FORM_PAGE, request.getMethod());
		HttpSession session = request.getSession();
		String lang = (String) session.getAttribute("language");
		User user = (User) session.getAttribute("user");
		try {
			Order order = OrderService.retrieveFormingOrder(user.getUserID());
			session.setAttribute("orderID", order.getOrderID());
			request.setAttribute("items", OrderService.retrieveOrderItems(order.getOrderID()));
			List<MenuSection> sections = MenuService.retrieveSections();
			if (!sections.isEmpty()) {
				int sectionID = 0;
				if (session.getAttribute("sectionID") != null) {
					sectionID = (int) session.getAttribute("sectionID");
				} else {
					sectionID = sections.get(0).getSectionID();
				}
				request.setAttribute("sectionID", sectionID);
				request.setAttribute("sections", sections);
				List<Dish> dishes = MenuService.retrieveActiveDishes(sectionID);
				if (!dishes.isEmpty()) {
					int dishID = 0;
					if (session.getAttribute("dishID") != null) {
						dishID = (int) session.getAttribute("dishID");
					} else {
						dishID = dishes.get(0).getDishID();
					}
					request.setAttribute("dishes", dishes);
					request.setAttribute("dishID", dishID);
					request.setAttribute("dish", MenuService.retrieveDish(dishID));
				}
			}
		} catch (ServiceException e) {
			LOG.error(MessageManager.getMessage(e.getMessage(), ""), e);
			session.setAttribute("errorPageMessage", MessageManager.getMessage(e.getMessage(), lang));
			uri = PageManager.getURI(ERROR_PAGE, request.getMethod());
		}
		return uri;
	}

}
