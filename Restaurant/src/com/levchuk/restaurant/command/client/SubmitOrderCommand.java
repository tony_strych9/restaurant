package com.levchuk.restaurant.command.client;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.entity.Item;
import com.levchuk.restaurant.service.OrderService;
import com.levchuk.restaurant.service.ServiceException;
import com.levchuk.restaurant.util.PageManager;
import com.levchuk.restaurant.util.MessageManager;

/**
 * Encapsulates the operation of submitting an order.
 * 
 * @author Anton Levchuk
 */
public class SubmitOrderCommand implements ActionCommand{
	
	private static final Logger LOG = Logger.getLogger(SubmitOrderCommand.class);
	private static final String MAIN_PAGE = "main";
	private static final String ORDER_FORM_PAGE = "orderForm";
	private static final String ERROR_PAGE = "error500";
	
	@Override
	public String execute(HttpServletRequest request) {
		String uri = null;
		HttpSession session = request.getSession();
		String lang = (String) session.getAttribute("language");
		int orderID = 0;
		if (request.getParameter("orderID") != null) {
			orderID = Integer.parseInt(request.getParameter("orderID"));
		}
		if (orderID != 0) {
			try {
				List<Item> items = OrderService.retrieveOrderItems(orderID);
				if (!items.isEmpty()) {
					if (OrderService.submitOrder(orderID)) {
						session.setAttribute("submittedMessage", 
								MessageManager.getMessage("message.order.submitted", lang));
					}
					uri = PageManager.getURI(MAIN_PAGE, request.getMethod());
				} else {
					session.setAttribute("emptySubmitError", 
							MessageManager.getMessage("error.order.emptySubmit", lang));
					uri = PageManager.getURI(ORDER_FORM_PAGE, request.getMethod());
				}
			} catch (ServiceException e) {
				LOG.error(MessageManager.getMessage(e.getMessage(), ""), e);
				session.setAttribute("errorPageMessage", MessageManager.getMessage(e.getMessage(), lang));
				uri = PageManager.getURI(ERROR_PAGE, request.getMethod());
			}
		}
		return uri;
	}

}
