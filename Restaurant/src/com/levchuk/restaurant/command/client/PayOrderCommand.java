package com.levchuk.restaurant.command.client;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.entity.Order;
import com.levchuk.restaurant.entity.enums.OrderStatus;
import com.levchuk.restaurant.service.OrderService;
import com.levchuk.restaurant.service.ServiceException;
import com.levchuk.restaurant.util.MessageManager;
import com.levchuk.restaurant.util.PageManager;

/**
 * Encapsulates the operation of paying for an order.
 * 
 * @author Anton Levchuk
 */
public class PayOrderCommand implements ActionCommand{
	
	private static final Logger LOG = Logger.getLogger(PayOrderCommand.class);
	private static final String CURRENT_ORDERS_PAGE = "currentOrders";
	private static final String ERROR_PAGE = "error500";

	@Override
	public String execute(HttpServletRequest request) {
		String uri = PageManager.getURI(CURRENT_ORDERS_PAGE, request.getMethod());
		HttpSession session = request.getSession();
		String lang = (String) session.getAttribute("language");
		int orderID = 0;
		if (request.getParameter("orderID") != null) {
			orderID = Integer.parseInt(request.getParameter("orderID"));
		}
		if (orderID != 0) {
			try {
				Order order = OrderService.retrieveOrderByID(orderID);
				if (order != null && order.getStatus().equals(OrderStatus.CONFIRMED)) {
					OrderService.updateOrderStatus(orderID, OrderStatus.PAID);
					session.setAttribute("orderPaidMessage", MessageManager.getMessage("message.order.paid", 
							lang, String.valueOf(orderID)));
				}
			} catch (ServiceException e) {
				LOG.error(MessageManager.getMessage(e.getMessage(), ""), e);
				session.setAttribute("errorPageMessage", MessageManager.getMessage(e.getMessage(), lang));
				uri = PageManager.getURI(ERROR_PAGE, request.getMethod());
			}
		} else {
			session.setAttribute("orderPaid", MessageManager.getMessage("error.order.paid", lang));
		}
		return uri;
	}

}
