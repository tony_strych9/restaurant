package com.levchuk.restaurant.tag;


import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;


/**
 * Retrieves a name of the current page.
 * 
 * @author Anton Levchuk
 *
 */
@SuppressWarnings("serial")
public class PageNameTag extends TagSupport {
	
	private static final Logger LOG = Logger.getLogger(PageNameTag.class);
	
	@Override
	public int doStartTag() throws JspException {
		HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
	    String uri = request.getRequestURI();
	    String pageName = uri.substring(uri.lastIndexOf("/") + 1, uri.lastIndexOf("."));
	    try {
	    	JspWriter out = pageContext.getOut();
			out.write(pageName);
		} catch (IOException e) {
			LOG.error(e);
		}
		return SKIP_BODY;
	}

	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}

}
