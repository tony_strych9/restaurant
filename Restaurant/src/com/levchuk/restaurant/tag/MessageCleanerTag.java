package com.levchuk.restaurant.tag;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * Clears the current session from message parameters.
 * 
 * @author Anton Levchuk
 *
 */
@SuppressWarnings("serial")
public class MessageCleanerTag extends TagSupport {
	
	private static final String MESSAGE = "message";
	private static final String ERROR = "error";

	@Override
	public int doStartTag() throws JspException {
		HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
		HttpSession session = request.getSession();
		Enumeration<String> sessionIterator = session.getAttributeNames();
		while (sessionIterator.hasMoreElements()) {
			String attributeName = sessionIterator.nextElement();
			if (attributeName.toLowerCase().contains(MESSAGE) || 
					attributeName.toLowerCase().contains(ERROR)) {
				session.removeAttribute(attributeName);
			}
		}
		return SKIP_BODY;
	}

	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}

}
