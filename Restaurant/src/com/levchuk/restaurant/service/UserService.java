package com.levchuk.restaurant.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import com.levchuk.restaurant.dao.DAOException;
import com.levchuk.restaurant.dao.UserDAO;
import com.levchuk.restaurant.entity.User;
import com.levchuk.restaurant.util.MessageManager;
import com.levchuk.restaurant.util.PasswordEncoder;

/**
 * UserService represents an application logic relating to user's information
 * and contains all appropriate methods to process that information
 * 
 * @author Anton Levchuk
 */
public final class UserService {
	
	private final static String ERROR_KEY_PREFIX = "error.service.";
	
	private final static ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("resources.regex");
	
	private UserService() {}
	
	/**
	 * Updates user's information.
	 * 
	 * @param user the User instance
	 * @return true if changes has been committed, false otherwise
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static boolean updateUser(User user) throws ServiceException {
		try {
			return new UserDAO().update(user);
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "updateUser", e);
		}
	}
	
	/**
	 * Retrieves the specified user.
	 * 
	 * @param userID the user's ID
	 * @return the User instance
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static User retrieveUser(int userID) throws ServiceException {
		try {
			return new UserDAO().findByID(userID);
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "retrieveUser", e);
		}
	}
	
	/**
	 * Retrieves a list of all users.
	 * 
	 * @return the list of users
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static List<User> retrieveUsers() throws ServiceException {
		try {
			List<User> users = new UserDAO().findAll();
			users.sort(Comparator.comparing(User::getLastVisit).reversed());
			return users;
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "retrieveUsers", e);
		}
	}
	
	/**
	 * Retrieves a user bound to the specified order.
	 * 
	 * @param orderID the order's ID
	 * @return the User instance
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static User retrieveUserByOrder(int orderID) throws ServiceException {
		try {
			return new UserDAO().findByOrderID(orderID);
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "retrieveUser", e);
		}
	}
	
	/**
	 * Checks if the user with such login and password exists.
	 * 
	 * @param login user's login
	 * @param password user's password
	 * @return the User instance if the user exists, null otherwise
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static User retrieveUser(String login, String password) throws ServiceException {
		User user = null;
		List<User> users = new ArrayList<User>();
		if (login != null && password != null) {
			try {
				users.addAll(new UserDAO().findByCriteria(login));
				if (!users.isEmpty() && 
						PasswordEncoder.encode(password).equals(users.get(0).getPassword())) {
					user = users.get(0);
				}
			} catch (DAOException e) {
				throw new ServiceException(ERROR_KEY_PREFIX + "retrieveUser", e);
			}
		}
		return user;
	}
	
	/**
	 * Creates a new user with the specified information.
	 * 
	 * @param user the User instance
	 * @return the created user
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static User createUser(User user) throws ServiceException {
		try {
			user.setUserID(new UserDAO().create(user));
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "createUser", e);
		}
		return user;
	}
	
	/**
	 * Deletes the specified user.
	 * 
	 * @param userID the user's ID
	 * @return true if the user has been deleted, false otherwise
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static boolean deleteUser(int userID) throws ServiceException {
		try {
			return new UserDAO().deleteByID(userID);
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "deleteUser", e);
		}
	}
	
	/**
	 * Validates the entered information when creating a new user.
	 * 
	 * @param login user's login
	 * @param password user's password
	 * @param repeatPassword password for confirmation
	 * @param firstName user's first name
	 * @param lastName user's last name
	 * @param email user's email
	 * @param lang current set language
	 * @return a map representing all errors occurred during the validation process
	 * @throws ServiceException if an DAOException was thrown during execution 
	 */
	public static Map<String, String> validateInfoToSignup(String login, String password, String repeatPassword, 
			String firstName, String lastName, String email, String lang) throws ServiceException {
		
		HashMap<String, String> errors = new HashMap<String, String>();

		if (login == null || "".equals(login)) {
			errors.put("emptyLoginError", MessageManager.getMessage("error.user.emptyLogin", lang));
		} else {
			if(!validateLogin(login)) {
				errors.put("loginRegexError", MessageManager.getMessage("error.user.loginRegex", lang));
			} else
				if (!validateLoginIsFree(login)) {
					errors.put("loginIsUsedError", MessageManager.getMessage("error.user.loginIsUsed", lang));
				}
		}
		
		if (password != null && !"".equals(password)) {
			if (!password.equals(repeatPassword)) {
				errors.put("passNotEqualError", MessageManager.getMessage("error.user.passNotEqual", lang));
			} else if (!validatePassword(password)) {
				errors.put("passRegexError", MessageManager.getMessage("error.user.passRegex", lang));
			}
		} else {
			errors.put("emptyPassError", MessageManager.getMessage("error.user.emptyPass", lang));
		}
		
		
		if (firstName != null && !"".equals(firstName)) {
			if (!validateName(firstName)) {
				errors.put("firstNameRegexError", MessageManager.getMessage("error.user.firstNameRegex", lang));
			}
		}
		if (lastName != null && !"".equals(lastName)) {
			if (!validateName(lastName)) {
				errors.put("lastNameRegexError", MessageManager.getMessage("error.user.lastNameRegex", lang));
			}
		}
		if (email == null || "".equals(email)) {
			errors.put("emptyEmailError", MessageManager.getMessage("error.user.emptyEmail", lang));
		} else {
			if(!validateEmail(email)) {
				errors.put("emailRegexError", MessageManager.getMessage("error.user.emailRegex", lang));
			} else
				if (!validateEmailIsFree(email)) {
					errors.put("emailUsedError", MessageManager.getMessage("error.user.emailIsUsed", lang));
				}
		}
		return errors;
	}
	
	/**
	 * Validates the entered information when editing a user.
	 * 
	 * @param password user's password
	 * @param repeatPassword password for confirmation
	 * @param firstName user's first name
	 * @param lastName user's last name
	 * @param email user's email
	 * @param lang current set language
	 * @return a map representing all errors occurred during the validation process
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static Map<String, String> validateInfoToEdit(String oldLogin, String newLogin, String password, String repeatPassword,
			String firstName, String lastName, String oldEmail, String newEmail, String lang) throws ServiceException {
		
		HashMap<String, String> errors = new HashMap<String, String>();
		
		if (newLogin == null || "".equals(newLogin)) {
			errors.put("emptyLoginError", MessageManager.getMessage("error.user.emptyLogin", lang));
		} else {
			if(!validateLogin(newLogin)) {
				errors.put("loginRegexError", MessageManager.getMessage("error.user.loginRegex", lang));
			} else
				if (!oldLogin.equals(newLogin) && !validateLoginIsFree(newLogin)) {
					errors.put("loginUsedError", MessageManager.getMessage("error.user.loginIsUsed", lang));
				}
		}
		
		if (password != null && !"".equals(password)) {
			if (!password.equals(repeatPassword)) {
				errors.put("passNotEqualError", MessageManager.getMessage("error.user.passNotEqual", lang));
			} else if (!validatePassword(password)) {
				errors.put("passRegexError", MessageManager.getMessage("error.user.passRegex", lang));
			}
		} 
		if (firstName != null && !"".equals(firstName)) {
			if (!validateName(firstName)) {
				System.out.println("name validation failed: " + firstName);
				errors.put("firstNameRegexError", MessageManager.getMessage("error.user.firstNameRegex", lang));
			}
		}
		if (lastName != null && !"".equals(lastName)) {
			if (!validateName(lastName)) {
				errors.put("lastNameRegexError", MessageManager.getMessage("error.user.lastNameRegex", lang));
			}
		}
		if (newEmail == null || "".equals(newEmail)) {
			errors.put("emptyEmailError", MessageManager.getMessage("error.user.emptyEmail", lang));
		} else {
			if(!validateEmail(newEmail)) {
				errors.put("emailRegexError", MessageManager.getMessage("error.user.emailRegex", lang));
			} else
				if (!oldEmail.equals(newEmail) && !validateEmailIsFree(newEmail)) {
					errors.put("emailUsedError", MessageManager.getMessage("error.user.emailIsUsed", lang));
				}
		}
		return errors;
	}
	
	/**
	 * Checks if the login is already being used.
	 * 
	 * @param login the login to check
	 * @return true if the login is not being used, false otherwise
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	private static boolean validateLoginIsFree(String login) throws ServiceException {
		try {
			return new UserDAO().findByCriteria(login).isEmpty();
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "validateLogin", e);
		}
	}
	
	/**
	 * Checks if the email is already being used.
	 * 
	 * @param email the email to check
	 * @return true if the email is not being used, false otherwise
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	private static boolean validateEmailIsFree(String email) throws ServiceException {
		try {
			return new UserDAO().findByEmail(email).isEmpty();
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "validateEmail", e);
		}
	}

	/**
	 * Validates the password for conformity to the corresponding regular expression.
	 * 
	 * @param password the password to validate
	 * @return false if validation failed
	 */
	private static boolean validatePassword(String password) {
		boolean result = true;
		if (password != null && !"".equals(password)) {
			result = password.matches(RESOURCE_BUNDLE.getString("user.password"));
		}
		return result;
	}
	
	/**
	 * Validates the name for conformity to the corresponding regular expression.
	 * 
	 * @param name the name to validate
	 * @return false if validation failed
	 */
	private static boolean validateName(String name) {
		boolean result = true;
		if (name != null && !"".equals(name)) {
			result = name.matches(RESOURCE_BUNDLE.getString("user.name"));
		}
		return result;
	}

	/**
	 * Validates the email for conformity to the corresponding regular expression.
	 * 
	 * @param email the email to validate
	 * @return false if validation failed
	 */
	private static boolean validateEmail(String email) {
		boolean result = true;
		if (email != null && !"".equals(email)) {
			result = email.matches(RESOURCE_BUNDLE.getString("user.email"));
		}
		return result;
	}
	
	/**
	 * Validates the login for conformity to the corresponding regular expression.
	 * 
	 * @param login the login to validate
	 * @return false if validation failed
	 */
	private static boolean validateLogin(String login) {
		boolean result = true;
		if (login != null && !"".equals(login)) {
			result = login.matches(RESOURCE_BUNDLE.getString("user.login"));
		}
		return result;
	}

}
