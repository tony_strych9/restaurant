package com.levchuk.restaurant.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.Part;

import org.apache.log4j.Logger;

import com.levchuk.restaurant.dao.DAOException;
import com.levchuk.restaurant.dao.DishDAO;
import com.levchuk.restaurant.dao.MenuDAO;
import com.levchuk.restaurant.entity.Dish;
import com.levchuk.restaurant.entity.MenuSection;
import com.levchuk.restaurant.entity.enums.DishStatus;
import com.levchuk.restaurant.util.MessageManager;

/**
 * MenuService represents an application logic relating to menu's information
 * and contains all appropriate methods to process that information.
 * 
 * @author Anton Levchuk
 */
public final class MenuService {

	private final static Logger LOG = Logger.getLogger(MenuService.class);
	private final static ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("resources.regex");
	private final static String IMAGES_DIRECTORY = "G:/images";
	private final static String ERROR_KEY_PREFIX = "error.service.";
	
	private MenuService() {}
	
	/**
	 * Uploads an image file supplied with the Part object.
	 * 
	 * @param imagePart the Part object
	 * @param dishName the dish's name
	 * @return the uploaded image file's full name with an extension 
	 * or an empty string if the image hasn't been uploaded
	 */
	public static String uploadImage(Part imagePart, String dishName) {
		String dishImage = null;
		if (imagePart != null) {
			try {
				String imageName = imagePart.getSubmittedFileName();
				int i = imageName.lastIndexOf('.');
				if (i > 0) {
				    String extension = imageName.substring(i);
				    dishImage = dishName.trim().replaceAll("\\s", "_") + extension;
				}
				File file = new File(IMAGES_DIRECTORY, dishImage);
				if (file.exists()) {
					file.delete();
				}
				try (InputStream fileContent = imagePart.getInputStream()) {
				    Files.copy(fileContent, file.toPath());
				}
				dishImage = dishImage + "?time=" + new Date().getTime();
			} catch (IOException e) {
				LOG.error(e);
				dishImage = "";
			}
		}
		return dishImage;
	}
	
	/**
	 * Creates a new dish in the menu.
	 * 
	 * @param dish the Dish object
	 * @return the created dish
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static Dish createDish(Dish dish) throws ServiceException {
		try {
			dish.setDishID(new DishDAO().create(dish));
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "createDish", e);
		}
		return dish;
	}
	
	/**
	 * Updates information about the specified dish.
	 * 
	 * @param dish the Dish object
	 * @return true if changes has been committed, false otherwise
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static boolean updateDish(Dish dish) throws ServiceException {
		try {
			return new DishDAO().update(dish);
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "updateDish", e);
		}
	}
	
	/**
	 * Retrieves a dish by the specified ID.
	 * 
	 * @param dishID the dish ID
	 * @return the Dish instance
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static Dish retrieveDish(int dishID) throws ServiceException {
		try {
			return new DishDAO().findByID(dishID);
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "retrieveDish", e);
		}
	}
	
	/**
	 * Returns a list of dishes present in the specified menu's section.
	 * 
	 * @param sectionID the section's ID
	 * @return the list of dishes
	 * @throws ServiceException if an DAOException was thrown during execution 
	 */
	public static List<Dish> retrieveDishes(int sectionID) throws ServiceException {
		List<Dish> dishes = new ArrayList<Dish>();
		try {
			dishes.addAll(new DishDAO().findByCriteria(sectionID));
			dishes.sort(Comparator.comparing(Dish::getName));
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "retrieveDishes", e);
		}
		return dishes;
	}
	
	/**
	 * Returns a list of only active dishes in the specified menu's section.
	 * 
	 * @param sectionID the section's ID
	 * @return the list of active dishes
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static List<Dish> retrieveActiveDishes(int sectionID) throws ServiceException {
		List<Dish> activeDishes = new ArrayList<Dish>();
		try {
			for (Dish dish : retrieveDishes(sectionID)) {
				if (dish.getStatus() == DishStatus.ACTIVE) {
					activeDishes.add(dish);
				}
			}
			activeDishes.sort(Comparator.comparing(Dish::getName));
		} catch (ServiceException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "retrieveDishes", e);
		}
		return activeDishes;
	}
	
	/**
	 * Returns a list of all menu's sections.
	 * 
	 * @return the list of menu's sections
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static List<MenuSection> retrieveSections() throws ServiceException {
		List<MenuSection> sections = new ArrayList<MenuSection>();
		try {
			sections.addAll(new MenuDAO().findAll());
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "retrieveSections", e);
		}
		return sections;
	}
	
	/**
	 * Validates entered information when creating a new dish.
	 * 
	 * @param name the dish's name
	 * @param price the dish's price
	 * @param description the dish's description
	 * @param image the image file's name
	 * @param lang the current language to localize output error messages
	 * @return a map representing all errors occurred during the validation process
	 * @throws ServiceException 
	 */
	public static Map<String, String> validateCreationInfo(String name, String price, String description, String image, String lang) throws ServiceException {
	
		HashMap<String, String> errors = new HashMap<String, String>();
		
		if (name == null || "".equals(name)) {
			errors.put("emptyCreateNameError", MessageManager.getMessage("error.menu.emptyName", lang));
		} else {
			if(!validateName(name)) {
				errors.put("createNameRegexError", MessageManager.getMessage("error.menu.nameRegex", lang));
			} else
				try {
					if (!validateNameIsFree(name)) {
						errors.put("createNameUsedError", MessageManager.getMessage("error.menu.nameIsUsed", lang));
					}
				} catch (DAOException e) {
					throw new ServiceException(ERROR_KEY_PREFIX + "validateName", e);
				}
		}
		
		if (description != null && !"".equals(description)) {
			if(!validateDescription(description)) {
				errors.put("createDescriptionRegexError", MessageManager.getMessage("error.menu.descriptionRegex", lang));
			}
		}
		
		if (price == null || "".equals(price)) {
			errors.put("emptyCreatePriceError", MessageManager.getMessage("error.menu.emptyPrice", lang));
		} else if (!validatePrice(price)) {
			errors.put("createPriceRegexError", MessageManager.getMessage("error.menu.priceRegex", lang));
		}
		
		if (image == null || "".equals(image)) {
			errors.put("emptyCreateImageError", MessageManager.getMessage("error.menu.emptyImage", lang));
		} else {
			if (!validateImage(image)) {
				errors.put("createImageRegexError", MessageManager.getMessage("error.menu.imageRegex", lang));
			}
		}
		return errors;
	}
	
	/**
	 * Validates entered information when editing a dish.
	 * 
	 * @param oldName the edited dish's name
	 * @param newName the newly defined dish's name
	 * @param price the dish's price
	 * @param description the dish's description
	 * @param image the image file's name
	 * @param lang the current language to localize output error messages
	 * @return a map representing all errors occurred during the validation process
	 * @throws ServiceException 
	 */
	public static Map<String, String> validateEditingInfo(String oldName, String newName, String price, String description, String image, String lang) throws ServiceException {
		
		HashMap<String, String> errors = new HashMap<String, String>();
		
		if (newName == null || "".equals(newName)) {
			errors.put("emptyEditNameError", MessageManager.getMessage("error.menu.emptyName", lang));
		} else {
			if(!validateName(newName)) {
				errors.put("editNameRegexError", MessageManager.getMessage("error.menu.nameRegex", lang));
			} else
				try {
					if (!oldName.equals(newName) && !validateNameIsFree(newName)) {
						errors.put("editNameUsedError", MessageManager.getMessage("error.menu.nameIsUsed", lang));
					}
				} catch (DAOException e) {
					throw new ServiceException(ERROR_KEY_PREFIX + "validateName", e);
				} 
		}
		
		if (description != null && !"".equals(description)) {
			if(!validateDescription(description)) {
				errors.put("editDescriptionRegexError", MessageManager.getMessage("error.menu.descriptionRegex", lang));
			}
		}
		
		if (price == null || "".equals(price)) {
			errors.put("emptyEditPriceError", MessageManager.getMessage("error.menu.emptyPrice", lang));
		} else if (!validatePrice(price)) {
			errors.put("editPriceRegexError", MessageManager.getMessage("error.menu.priceRegex", lang));
		}
		
		if (image != null) {
			if (!validateImage(image)) {
				errors.put("editImageRegexError", MessageManager.getMessage("error.menu.imageRegex", lang));
			}
		}
		return errors;
	}
	
	/**
	 * Checks if the name is already being used.
	 * 
	 * @param name the name to validate
	 * @return false if name is being used, true otherwise
	 * @throws DAOException 
	 */
	private static boolean validateNameIsFree(String name) throws DAOException {
		return new DishDAO().findByName(name) == null;
	}
	
	/**
	 * Validates the name for conformity to the corresponding regular expression.
	 * 
	 * @param name the name to validate
	 * @return false if validation failed
	 */
	private static boolean validateName(String name) {
		boolean result = true;
		if (name != null && !"".equals(name)) {
			result = name.matches(RESOURCE_BUNDLE.getString("dish.name"));
		}
		return result;
	}
	
	/**
	 * Validates the price for conformity to the corresponding regular expression.
	 * 
	 * @param price the price to validate
	 * @return false if validation failed
	 */
	private static boolean validatePrice(String price) {
		boolean result = true;
		if (price != null && !"".equals(price)) {
			result = price.matches(RESOURCE_BUNDLE.getString("dish.price"));
		}
		return result;
	}
	
	/**
	 * Validates the description for conformity to the corresponding regular expression.
	 * 
	 * @param description the description to validate
	 * @return false if validation failed
	 */
	private static boolean validateDescription(String description) {
		boolean result = true;
		if (description != null && !"".equals(description)) {
			result = description.matches(RESOURCE_BUNDLE.getString("dish.description"));
		}
		return result;
	}
	
	/**
	 * Validates the image's name.
	 * 
	 * @param description the description to validate
	 * @return false if validation failed
	 */
	private static boolean validateImage(String image) {
		boolean result = true;
		if (image != null && !"".equals(image)) {
			result = image.matches(RESOURCE_BUNDLE.getString("dish.image"));
		}
		return result;
	}
	
}
