package com.levchuk.restaurant.service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import org.apache.commons.lang.LocaleUtils;

import com.levchuk.restaurant.dao.DAOException;
import com.levchuk.restaurant.dao.ItemDAO;
import com.levchuk.restaurant.dao.OrderDAO;
import com.levchuk.restaurant.dao.UserDAO;
import com.levchuk.restaurant.entity.Item;
import com.levchuk.restaurant.entity.Order;
import com.levchuk.restaurant.entity.User;
import com.levchuk.restaurant.entity.enums.DishStatus;
import com.levchuk.restaurant.entity.enums.OrderStatus;

/**
 * OrderService represents an application logic relating to order's information
 * and contains all appropriate methods to process that information.
 * 
 * @author Anton Levchuk
 */
public final class OrderService {
	
	private final static String ERROR_KEY_PREFIX = "error.service.";
	
	private OrderService() {}
	
	/**
	 * Retrieves the specified item.
	 * 
	 * @param itemID the item's ID
	 * @return the Item instance
	 * @throws ServiceException if an DAOException was thrown during execution 
	 */
	public static Item retrieveItem(int itemID) throws ServiceException {
		try {
			return new ItemDAO().findByID(itemID);
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "retrieveItem", e);
		}
	}
	
	/**
	 * Sets the specified order's status to submitted.
	 * 
	 * @param orderID the order's ID
	 * @return true if the order has been submitted, false otherwise
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static boolean submitOrder(int orderID) throws ServiceException {
		boolean result = false;
		Order order = OrderService.retrieveOrderByID(orderID);
		if (order != null) {
			order.setStatus(OrderStatus.SUBMITTED);
			order.setTime(new Date());
			try {
				result = new OrderDAO().update(order);
			} catch (DAOException e) {
				throw new ServiceException(ERROR_KEY_PREFIX + "updateOrderStatus", e);
			}
		}
		return result;
	}
	
	/**
	 * Retrieves the specified order.
	 * 
	 * @param orderID the order's ID
	 * @return the Order instance
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static Order retrieveOrderByID(int orderID) throws ServiceException {
		try {
			return new OrderDAO().findByID(orderID);
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "retrieveOrder", e);
		}
	}
	
	/**
	 * Retrieves the order being formed by the specified user. 
	 * If the order doesn't exist creates a new one.
	 * 
	 * @param userID the user's ID
	 * @return the Order instance
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static Order retrieveFormingOrder(int userID) throws ServiceException {
		Order order = null;
		List<Order> orders = retrieveOrdersByUserAndStatus(userID, OrderStatus.FORMING);
		if (!orders.isEmpty()) {
			order = orders.get(0);
		} else {
			order = new Order();
			order.setUserID(userID);
			order.setStatus(OrderStatus.FORMING);
			order.setTime(new Date());
			try {
				order.setOrderID(new OrderDAO().create(order));
			} catch (DAOException e) {
				throw new ServiceException(ERROR_KEY_PREFIX + "retrieveOrder", e);
			}
		}
		return order;
	}
	
	/**
	 * Deletes the specified order and all items it contains.
	 * 
	 * @param orderID the order's ID
	 * @return true if the order has been deleted, false otherwise
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static boolean deleteOrder(int orderID) throws ServiceException {
		boolean result = false;
		try {
			if (new ItemDAO().deleteByCriteria(orderID) && new OrderDAO().deleteByID(orderID)) {
				result = true;
			}
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "deleteOrder", e);
		}
		return result;
	}
	
	/**
	 * Deletes all items in the specified order.
	 * 
	 * @param orderID the order's ID
	 * @return true if the order has been cleared, false otherwise
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static boolean clearOrder(int orderID) throws ServiceException {
		try {
			return new ItemDAO().deleteByCriteria(orderID);
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "clearOrder", e);
		}
	}
	
	/**
	 * Updates a status of the specified order.
	 * 
	 * @param orderID the order's ID
	 * @param status the order's status
	 * @return true if the status has been updated, false otherwise
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static boolean updateOrderStatus(int orderID, OrderStatus status) throws ServiceException {
		try {
			return new OrderDAO().updateStatus(orderID, status);
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "updateOrderStatus", e);
		}
	}
	
	/**
	 * Retrieves an order by the specified user and order's status.
	 * 
	 * @param userID the user's ID
	 * @param status the order's status
	 * @return the list of orders
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static List<Order> retrieveOrdersByUserAndStatus(int userID, OrderStatus status) throws ServiceException {
		List<Order> orders = new ArrayList<Order>();
		try {
			orders.addAll(new OrderDAO().findByCriteria(userID));
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "retrieveOrder", e);
		}
		return orders.stream().filter(or -> or.getStatus().equals(status)).collect(Collectors.toList());
	}
	
	/**
	 * Retrieves orders by the specified order's status.
	 * 
	 * @param status the order's status
	 * @return the list of orders
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static List<Order> retrieveOrdersByStatus(OrderStatus status) throws ServiceException {
		List<Order> orders = new ArrayList<Order>();
		try {
			orders.addAll(new OrderDAO().findByStatus(status));
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "retrieveOrder", e);
		}
		orders.sort(Comparator.comparing(Order::getTime).reversed());
		return orders;
	}
	
	/**
	 * Retrieves all items contained in the specified order.
	 * @param orderID the order' ID
	 * @return the list of items
	 * @throws ServiceException if an DAOException was thrown during execution 
	 */
	public static List<Item> retrieveOrderItems(int orderID) throws ServiceException {
		try {
			List<Item> items = new ItemDAO().findByCriteria(orderID);
			items.sort(Comparator.comparing(Item::getItemID));
			return items;
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "retrieveItems", e);
		}
	}
	
	/**
	 * Adds the specified dish to the order by incrementing the quantity of an existing item or
	 * creating a new item.
	 * 
	 * @param orderID the order's ID
	 * @param dishID the dish's ID
	 * @return true if the dish has been added, false otherwise
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static boolean addDishToOrder(int orderID, int dishID) throws ServiceException {
		boolean result = false;
		Item item;
		try {
			item = new ItemDAO().findByOrderIDAndDishID(orderID, dishID);
			if (item == null) {
				item = new Item();
				item.setOrderID(orderID);
				item.setDishID(dishID);
				item.setQuantity(1);
				if (new ItemDAO().create(item) > 0) {
					result = true;
				}
			} else {
				item.setQuantity(item.getQuantity() + 1);
				result = new ItemDAO().update(item);
			}
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "addDishToOrder", e);
		}
		return result;
	}
	
	/**
	 * Removes a dish from the order by decrementing the quantity of an existing item
	 * or deleting the item completely.
	 * 
	 * @param orderID the order's ID
	 * @param dishID the dish's ID
	 * @return true if the dish has been deleted, false otherwise
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static boolean removeDishFromOrder(int orderID, int dishID) throws ServiceException {
		boolean result = false;
		ItemDAO itemDAO = new ItemDAO();
		Item item;
		try {
			item = itemDAO.findByOrderIDAndDishID(orderID, dishID);
			if (item != null) {
				if (item.getQuantity() > 1) {
					item.setQuantity(item.getQuantity() - 1);
					result = itemDAO.update(item);
				} else {
					result = itemDAO.deleteByID(item.getItemID());
				}
			}
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "removeDishFromOrder", e);
		}
		return result;
	}

	/**
	 * Retrieves all client's orders pending for paycheck or confirmation.
	 * 
	 * @param userID the user's ID
	 * @return the list of orders
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static List<Order> retrieveCurrentOrders(int userID) throws ServiceException {
		List<Order> orders = retrieveOrdersByUserAndStatus(userID, OrderStatus.CONFIRMED);
		orders.addAll(retrieveOrdersByUserAndStatus(userID, OrderStatus.SUBMITTED));
		orders.sort(Comparator.comparing(Order::getStatus).reversed().
				thenComparing(Order::getTime).reversed());
		return orders;
	}
	
	/**
	 * Retrieves all ongoing orders by all clients.
	 * 
	 * @return the list of orders
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static List<Order> retrieveAllCurrentOrders() throws ServiceException {
		List<Order> orders = retrieveOrdersByStatus(OrderStatus.CONFIRMED);
		orders.addAll(retrieveOrdersByStatus(OrderStatus.SUBMITTED));
		orders.addAll(retrieveOrdersByStatus(OrderStatus.CANCELED));
		orders.sort(Comparator.comparing(Order::getStatus).reversed().
				thenComparing(Order::getTime).reversed());
		return orders;
	}
	
	/**
	 * Builds a string representation of an order information to be viewed by the administrator.
	 * 
	 * @param orderID the order's ID
	 * @param lang the language
	 * @return the order's information
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static String buildInfoByAdmin(int orderID, String lang) throws ServiceException {
		ResourceBundle bundle = ResourceBundle.getBundle("resources.pagecontent", LocaleUtils.toLocale(lang));
		DecimalFormat formatter = new DecimalFormat("#0.00");
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
		List<Item> items = new ArrayList<Item>();
		User user = null;
		Order order = null;
		try {
			items.addAll(new ItemDAO().findByCriteria(orderID));
			user = new UserDAO().findByOrderID(orderID);
			order = new OrderDAO().findByID(orderID);
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "buildInfo", e);
		}
		StringBuilder sb = new StringBuilder();
		sb.append(bundle.getString("order.order") + " #" + orderID);
		sb.append("\n" + bundle.getString("order.login") + ": ");
		if (user == null) {
			sb.append(bundle.getString("order.userNotFound"));
		} else {
			sb.append(user.getLogin());
		}
		if (order == null) {
			sb.append("\n" + bundle.getString("order.orderNotFound"));
		} else {
			if (order.getStatus().equals(OrderStatus.CANCELED)) {
				sb.append("\n" + bundle.getString("order.canceled") + ".\n\n");
			} else {
				sb.append("\n" + bundle.getString("order.submitted") + ": ");
				sb.append(format.format(order.getTime()) + "\n\n");
			}
			if (items.isEmpty()) {
				sb.append(bundle.getString("order.empty"));
			} else {
				int cnt = 1;
				double totalPrice = 0;
				int totalQuantity = 0;
				for (Item item : items) {
					sb.append(cnt++ + ") \"" + item.getDishName() + "\" [" + 
							bundle.getString("order.quantity") + ": " + item.getQuantity());
					if (item.getDishStatus() == DishStatus.INACTIVE) {
						sb.append("] - " + bundle.getString("order.inactive") + "\n");
					} else {
						sb.append("; " + bundle.getString("order.price") + ": ");
						sb.append(item.getTotalPrice() + " $]\n");
						totalPrice = totalPrice + item.getTotalPrice();
						totalQuantity = totalQuantity + item.getQuantity();
					}
				}
				sb.append("\n" + bundle.getString("order.totalQuantity") + ": " + totalQuantity);
				sb.append("\n" + bundle.getString("order.totalPrice") + ": ");
				sb.append(formatter.format(totalPrice) + " $");
			}
		}
		return sb.toString();
	}
	
	/**
	 * Builds a string representation of an order information to be viewed by the client.
	 * 
	 * @param orderID the order's ID
	 * @param lang the language
	 * @return the order's information
	 * @throws ServiceException if an DAOException was thrown during execution
	 */
	public static String buildInfoByClient(int orderID, String lang) throws ServiceException {
		ResourceBundle bundle = ResourceBundle.getBundle("resources.pagecontent", LocaleUtils.toLocale(lang));
		DecimalFormat formatter = new DecimalFormat("#0.00");
		List<Item> items = new ArrayList<Item>();
		try {
			items.addAll(new ItemDAO().findByCriteria(orderID));
		} catch (DAOException e) {
			throw new ServiceException(ERROR_KEY_PREFIX + "buildInfo", e);
		}
		StringBuilder sb = new StringBuilder();
		sb.append(bundle.getString("order.order") + " #" + orderID + ":\n\n");
		int cnt = 1;
		double totalPrice = 0;
		int totalQuantity = 0;
		for (Item item : items) {
			sb.append(cnt++ + ") \"" + item.getDishName() + "\" [");
			sb.append(bundle.getString("order.quantity") + ": " + item.getQuantity() + "; ");
			sb.append(bundle.getString("order.price") + ": " + item.getTotalPrice() + "$]\n");
			totalPrice = totalPrice + item.getTotalPrice();
			totalQuantity = totalQuantity + item.getQuantity();
		}
		sb.append("\n" + bundle.getString("order.totalQuantity") + ": " + totalQuantity);
		sb.append("\n" + bundle.getString("order.totalPrice") + ": ");
		sb.append(formatter.format(totalPrice) + " $");
		return sb.toString();
	}

}
