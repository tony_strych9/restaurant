package com.levchuk.restaurant.controller;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.levchuk.restaurant.command.ActionCommand;
import com.levchuk.restaurant.command.factory.CommandFactory;
import com.levchuk.restaurant.util.MessageManager;
import com.levchuk.restaurant.util.PageManager;

/**
 * Functions as a controller that is responsible for processing incoming requests 
 * and sending a response back to the client.
 * 
 * @author Anton Levchuk
 *
 */
@WebServlet("/controller")
@MultipartConfig
public class ControllerServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private static final String ERROR_PAGE = "error500";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uri = processRequest(request);
		RequestDispatcher dispatcher = request.getRequestDispatcher(uri);
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uri = processRequest(request);
		response.sendRedirect(request.getContextPath() + uri);
	}
	
	/**
	 * Passes a request to the command layer for further input processing.
	 * 
	 * @param request the request
	 * @return the URI
	 */
	private String processRequest(HttpServletRequest request) {
		CommandFactory factory = new CommandFactory();
		ActionCommand command = factory.getCommand(request);
		String uri = command.execute(request);
		return processURI(uri, request);
	}
	
	/**
	 * Handles empty URIs if such present.
	 * 
	 * @param uri the URI
	 * @param request the request
	 * @return the processed URI
	 */
	private String processURI(String uri, HttpServletRequest request) {
		if (uri == null) {
			HttpSession session = request.getSession();
			String language = (String) session.getAttribute("language");
			uri = PageManager.getURI(ERROR_PAGE, request.getMethod());
			session.setAttribute("errorPageMessage", 
					MessageManager.getMessage("error.common.nullURI", language));
		}
		return uri;
	}
	
}
