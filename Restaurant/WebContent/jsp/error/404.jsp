<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.language}" scope="session" />
<fmt:setBundle basename="resources.pagecontent"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <title><fmt:message key="error.title"/> </title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/common.css">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
  <div id="container">
  
    <div id="header">
      <%@include file="/jsp/common/header.jspf" %>
    </div>
    
    <div id="content">
      <h1>ERROR ${pageContext.errorData.statusCode}</h1><br>
      <h2><fmt:message key="error.pageNotFound"/>:</h2><br> 
      <h3>${pageContext.errorData.requestURI}</h3><br><br>
      <h3><fmt:message key="error.prompt1"/> 
      <a href="javascript:history.back();"><fmt:message key="error.prompt2"/></a>
      <fmt:message key="error.prompt4"/> 
      <a href="${pageContext.request.contextPath}/jsp/common/main.jsp">
        <fmt:message key="error.prompt5"/>
      </a>.</h3>
    </div>
  </div>
  
  <div id="footer">
    <%@include file="/jsp/common/footer.jspf" %>
  </div>
</body>
</html>