<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session" />
<fmt:setBundle basename="resources.pagecontent"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/common.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigationBar.css">
<title><fmt:message key="main.title"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<c:remove var="orderID" scope="session" />
<c:remove var="sectionID" scope="session" />
<c:remove var="dishID" scope="session" />
<c:remove var="showInactiveBox" scope="session" />
</head>

<body>
  <div id="container">
    <div id="header">
      <%@include file="/jsp/common/header.jspf" %>
    </div>
    
    <div id="nav">
      <c:if test="${sessionScope.user.role ne 'ADMINISTRATOR'}">
        <center>
	      <a href="${pageContext.request.contextPath}/controller?command=show_menu">
		    <img src="${pageContext.request.contextPath}/images/menu.png" style="width:170px" height="100px">
		  </a>
        </center>
      </c:if>
      <%@ include file="/jsp/common/navigationBar.jspf" %>
    </div>

	<div id="content">
      <div id="left-content">
        <font color="red">${authorizedError}</font> 
 	    <font color="green">${submittedMessage} ${loginMessage}</font>
      </div>
    </div>
  </div>

  <div id="footer">
    <%@include file="/jsp/common/footer.jspf" %>
  </div>
  
  <ctg:removeMessages/>
</body>
</html>   