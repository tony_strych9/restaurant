<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session" />
<fmt:setBundle basename="resources.pagecontent"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/common.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigationBar.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="signup.title"/></title>
<style type="text/css">
label {color: red;}
</style>
</head>
<body>
  <div id="container">
    <div id="header">
      <%@include file="/jsp/common/header.jspf" %>
    </div>
    
    <div id="nav">
      <%@ include file="/jsp/common/navigationBar.jspf" %><br>
    </div>
    
    <div id="content">
      <div id="left-content">
  	    <p id="prompt"><fmt:message key="signup.prompt1"/></p>
	    <form name="signupForm" action="${pageContext.request.contextPath}/controller" method="POST">
	      <fmt:message key="signup.login"/> *:<br>
	      <input type="text" name="login" required> 
	      <label>${emptyLoginError} ${loginIsUsedError} ${loginRegexError}</label><br>
	      
		  <br><fmt:message key="signup.password"/> *:<br>
		  <input type="password" name="password" required> 
		  <label>${emptyPassError} ${passRegexError}</label><br>
		  
		  <br><fmt:message key="signup.confirmPassword"/> *:<br>
		  <input type="password" name="repeatPassword" required> 
		  <label>${passNotEqualError}</label><br>
		  
		  <br><fmt:message key="signup.firstName"/>:<br>
		  <input type="text" name="firstName" value=""> 
		  <label>${firstNameRegexError}</label><br>
		  
		  <br><fmt:message key="signup.lastName"/>:<br>
		  <input type="text" name="lastName" value=""> 
		  <label>${lastNameRegexError}</label><br>
		  
		  <br><fmt:message key="signup.email"/> *:<br>
		  <input type="text" name="email" value=""> 
		  <label>${emailRegexError}</label><br>
		
		  <c:if test="${'ADMINISTRATOR' eq sessionScope.user.role}">
		    <br><fmt:message key="signup.role"/>:
		    <br><input type="radio" name="role" value="CLIENT" checked="checked"> <fmt:message key="signup.client"/>&nbsp;
		    <input type="radio" name="role" value="ADMINISTRATOR"> <fmt:message key="signup.admin"/>
		  </c:if><br>
		
		  <br><input type="submit" value=<fmt:message key="signup.submitButton"/>>
		  <input type="hidden" name="command" value="signup"><br><br>
		  	   
		  <c:if test="${sessionScope.user == null}">
	        <br><fmt:message key="signup.prompt2"/> 
	        <a href="${pageContext.request.contextPath}/jsp/common/login.jsp"><fmt:message key="signup.prompt3"/></a><br><br>
	      </c:if>
	      <font color="red">${signupError}</font>
	      <font color="green">${signupMessage}</font>
	    </form>
	  </div>
	  
	  <div id="right-content">
	  </div>
    </div>
  </div>
  
  <div id="footer">
    <%@include file="/jsp/common/footer.jspf" %>
  </div>

  <ctg:removeMessages/>
</body>
</html>