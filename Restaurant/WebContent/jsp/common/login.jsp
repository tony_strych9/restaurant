<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session" />
<fmt:setBundle basename="resources.pagecontent"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/common.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigationBar.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="login.title"/></title>
</head>
<body>
  <div id="container">
    <div id="header">
      <%@include file="/jsp/common/header.jspf" %>
    </div>
    
    <div id="nav">
      <%@ include file="/jsp/common/navigationBar.jspf" %><br>
    </div>
    
    <div id="content">
      <div id="left-content">
  	    <p id="prompt"><fmt:message key="login.prompt"/></p>
	    <form name="signinForm" action="${pageContext.request.contextPath}/controller" method="POST">
	      <input type="hidden" name="command" value="login">
	      <fmt:message key="login.login"/>:<br><input type="text" name="login" value=""><br><br>
	      <fmt:message key="login.password"/>:<br><input type="password" name="password" value=""><br><br>
		  <input type="submit" value="<fmt:message key="login.loginButton"/>">
	    </form><br><br>
	    <font color="red">${loginError}</font>
	  </div>
    </div>
  </div>
  
  <div id="footer">
    <%@include file="/jsp/common/footer.jspf" %>
  </div>
  
  <ctg:removeMessages/>
</body>
</html>