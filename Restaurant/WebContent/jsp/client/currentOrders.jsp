<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session" />
<fmt:setBundle basename="resources.pagecontent"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/common.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigationBar.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/manageBar.css">
<title><fmt:message key="currentOrders.title"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript">
function setOrderID() {
	var e = document.getElementById("ordersList");
	var strUser = e.options[e.selectedIndex].value;
	document.getElementById("orderIDElement").value = strUser;
}
</script>
</head>

<body>
  <div id="container">
    <div id="header">
      <%@include file="/jsp/common/header.jspf" %>
    </div>

    <div id="nav">
      <%@ include file="/jsp/common/navigationBar.jspf" %>
    </div>

    <div id="mbar">
      <%@ include file="/jsp/client/profileBar.jspf" %>
    </div>

    <div id="content">
      <div id="left-content">
  	    <p id="caption"><fmt:message key="currentOrders.ordersList"/>:</p>
  	    <form action="${pageContext.request.contextPath}/controller">
		    <div style="width:200x;">
		      <select id="ordersList" name="orderID" size="10" style="width: 500px;" onchange="this.form.submit()">
			    <c:if test="${empty orders}">
			      <option disabled="disabled"><fmt:message key="currentOrders.noOrders"/>
			    </c:if>
			    <c:forEach var="order" items="${orders}">
			      <option value="${order.orderID}"
			        <c:if test="${order.orderID eq orderID}">selected</c:if>
				    >
				    <fmt:message key="currentOrders.order"/> #${order.orderID} 
				    [<fmt:message key="currentOrders.submitted"/>: <fmt:formatDate type="both" 
            value="${order.time}"/>] - 
				    <c:choose>
				    <c:when test="${'CONFIRMED' eq order.status}">
				      <fmt:message key="currentOrders.payPending"/>
				    </c:when>
				    <c:when test="${'SUBMITTED' eq order.status}">
				      <fmt:message key="currentOrders.confirmPending"/>
				    </c:when>
				    </c:choose>
				    <c:set var="counter" value="${counter + 1}"/>
			      </option>
		        </c:forEach>
		  	  </select>
		    </div>
		  <center>
		    <input type="hidden" name="command" value="show_order_info">
		    <input type="hidden" name="pageName" value=<ctg:pageName/>>
		  </center>
	    </form><br>
	    <font color="green">${orderPaidMessage}${orderCanceledMessage}</font><font color="red">${orderPaidError}${orderCanceledError}</font>
      </div>
      
      <div id="right-content">
        <c:if test="${orderInfo != null}">
          <p id="caption"><fmt:message key="currentOrders.orderInfo"/>:</p>
          <p><textarea rows="15" cols="50" readonly>${orderInfo}</textarea></p><br>
          <center>
            <form action="${pageContext.request.contextPath}/controller" method="POST">
              <input type="hidden" name="orderID" id="orderIDElement" >
              <c:choose>
                <c:when test="${'CONFIRMED' eq requestScope.status}">
                  <button name="command" value="pay_order" style="width:100px" onclick="setOrderID()">
		          <fmt:message key="currentOrders.payButton"/></button>
                </c:when>
                <c:when test="${'SUBMITTED' eq requestScope.status}">
                  <button name="command" value="cancel_order" style="width:100px" onclick="setOrderID()">
		          <fmt:message key="currentOrders.cancelButton"/></button>
                </c:when>
              </c:choose>
            </form>
          </center>
        </c:if>
      </div>
  	</div>
  </div>
  
  <div id="footer">
    <%@include file="/jsp/common/footer.jspf" %>
  </div>
  
  <ctg:removeMessages/>
</body>
</html>  