<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session" />
<fmt:setBundle basename="resources.pagecontent"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/common.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigationBar.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/manageBar.css">
<title><fmt:message key="profileEditor.title"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript">
function enableButton(){
        document.editProfile.button.disabled=false
}
</script>
<style type="text/css">
label {color:red;}
</style>
</head>

<body>
  <div id="container">
  <% System.out.println("JSP: " + pageContext.getRequest().getCharacterEncoding()); %>
  <c:set var="test" value="тест" scope="request"/>
    <div id="header">
      <%@include file="/jsp/common/header.jspf" %>
    </div>

    <div id="nav">
      <%@ include file="/jsp/common/navigationBar.jspf" %>
    </div>

    <div id="mbar">
      <%@ include file="/jsp/client/profileBar.jspf" %>
    </div>

    <div id="content">
      <div id="left-content">
  	    <p id="prompt"><fmt:message key="profileEditor.prompt"/><p>
  	    <form name="editProfile" action="${pageContext.request.contextPath}/controller" 
  	    method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
		  <fmt:message key="profileEditor.password"/>:<br>
		  <input type="password" name="password" onchange="enableButton()">
		  <label>${emptyPassError} ${passRegexError}</label><br>
		
		  <br><fmt:message key="profileEditor.confirmPassword"/>:<br>
		  <input type="password" name="repeatPassword">
		  <label>${passNotEqualError}</label><br>
		
		  <br><fmt:message key="profileEditor.firstName"/>:<br>
		  <input type="text" name="firstName" value="${user.firstName}">
		  <label>${firstNameRegexError}</label><br>
		
		  <br><fmt:message key="profileEditor.lastName"/>:<br>
		  <input type="text" name="lastName" value="${user.lastName}">
		  <label>${lastNameRegexError}</label><br>
		
		  <br><fmt:message key="profileEditor.email"/>:<br>
		  <input type="text" name="email" value="${user.email}">
		  <label>${emptyEmailError} ${emailRegexError} ${emailUsedError}</label><br>
		
		  <input type="hidden" name="test" value="${test}" >
		  <input type="hidden" name="command" value="edit_profile" >
		  <br><input type="submit" value=<fmt:message key="profileEditor.submitButton"/> name="button"><br><br>
	    </form>
	    <font color="green">${editedMessage}</font>
	    <font color="red">${editedError}</font>
	  </div>
    </div>
  </div>
  
  <div id="footer">
    <%@include file="/jsp/common/footer.jspf" %>
  </div>
  
  <ctg:removeMessages/>
</body>
</html>  