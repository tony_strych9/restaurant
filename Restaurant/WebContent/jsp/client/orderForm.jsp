<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session" />
<fmt:setBundle basename="resources.pagecontent"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/common.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigationBar.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="orderForm.title"/></title>
</head>
<body>
  <div id="container">
  
    <div id="header">
      <%@include file="/jsp/common/header.jspf" %>
    </div>
    
    <div id="nav">
      <%@ include file="/jsp/common/navigationBar.jspf" %><br>
    </div>
    
    <div id="content">
      <div id="left-content">
	    <p id="caption"><fmt:message key="orderForm.dishesList"/></p>
	    <%@include file="/jsp/common/order.jspf" %>
	    <table>
	      <tr>
	        <td>
	          <form action="${pageContext.request.contextPath}/controller" method="POST">
		        <input type="hidden" name="command" value="submit_order">
		        <input type="hidden" name="orderID" value="${sessionScope.orderID}">
		        <input type="submit" value=<fmt:message key="orderForm.submitButton"/> style="width:100px">
	          </form>
	        </td>
	        <td>
	          <form action="${pageContext.request.contextPath}/controller" method="POST">
		        <input type="hidden" name="command" value="clear_order">
		        <input type="hidden" name="orderID" value="${sessionScope.orderID}">
		        <input type="submit" value=<fmt:message key="orderForm.clearButton"/> style="width:100px">
	          </form>
	        </td>
	      </tr>
	    </table>
	    <br><font color="red">${emptySubmitError} ${deletedError} ${submittedError}</font>
	    <font color="green">${deletedMessage}</font>
	  </div>
	  
	  <div id="right-content">
	    <p id="prompt"><fmt:message key="orderForm.menuPrompt"/></p>
	    <%@ include file="/jsp/common/menu.jspf" %>
	    <font color="green">${dishAddedMessage}</font>
      </div>
    </div>
  </div>
  
  <div id="footer">
    <%@include file="/jsp/common/footer.jspf" %>
  </div>
  
  <ctg:removeMessages/>
</body>
</html>