<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session" />
<fmt:setBundle basename="resources.pagecontent"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/common.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigationBar.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/manageBar.css">
<title><fmt:message key="usersManagement.title"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
label {color:red;}
fieldset {
	padding:15px 15px 15px 15px;
}
fieldset p {
	padding-bottom:10px;
}
fieldset legend {
	font-size: 22px;
}
</style>

</head>

<body>

  <div id="container">
  
    <div id="header">
      <%@include file="/jsp/common/header.jspf" %>
    </div>

    <div id="nav">
      <%@ include file="/jsp/common/navigationBar.jspf" %>
    </div>

    <div id="mbar">
      <%@ include file="/jsp/admin/manageBar.jspf" %>
    </div>

    <div id="content">
      <div id="left-content">
        <p id="caption"><fmt:message key="usersManagement.usersList"/>:</p>
  	    <form action="${pageContext.request.contextPath}/controller">
		    <div style="width:200x;">
		      <select id="usersList" name="userID" size="10" style="width: 400px;" onchange="this.form.submit()">
			    <c:if test="${empty users}">
			      <option disabled="disabled"><fmt:message key="usersManagement.noUsers"/>
			    </c:if>
			    <c:forEach var="user" items="${users}">
			      <option value="${user.userID}"
			        <c:if test="${user.userID eq userID}">selected</c:if>
				    >
					${user.login} : [<fmt:message key="usersManagement.lastVisit"/>: 
					<fmt:formatDate type="both" value="${user.lastVisit}"/>]
				    <c:set var="counter" value="${counter + 1}"/>
			      </option>
		        </c:forEach>
		  	  </select>
		    </div>
		  <center>
		    <input type="hidden" name="command" value="manage_users">
		  </center>
	    </form><br>
      
      </div>
      
      <div id="right-content">
	    <form action="${pageContext.request.contextPath}/controller" method="POST">
	      <fieldset>
	        <legend><fmt:message key="usersManagement.editUser"/></legend>
	      
	        <br><p><fmt:message key="usersManagement.editLogin"/>:<br> 
	        <input type="text" name="login" value="${user.login}">
	        <label>${emptyLoginError} ${loginRegexError} ${loginUsedError}</label><br></p>
	       
	        <p><fmt:message key="usersManagement.editPassword"/>:<br> 
	        <input type="password" name="password" value="">
	        <label>${emptyPassError} ${passRegexError}</label><br></p>
	        
	        <p><fmt:message key="usersManagement.repeatPassword"/>:<br> 
	        <input type="password" name="repeatPassword" value="">
	        <label>${passNotEqualError}</label><br></p>
	      
	        <p><fmt:message key="usersManagement.editFirstName"/>:<br> 
	        <input type="text" name="firstName" value="${user.firstName}">
	        <label>${firstNameRegexError}</label><br></p>
	        
	        <p><fmt:message key="usersManagement.editLastName"/>:<br> 
	        <input type="text" name="lastName" value="${user.lastName}">
	        <label>${lastNameRegexError}</label><br></p>
	        
	        <p><fmt:message key="usersManagement.editEmail"/>:<br> 
	        <input type="text" name="email" value="${user.email}">
	        <label>${emptyEmailError} ${emailRegexError} ${emailUsedError}</label><br></p>
	        
	        <p><fmt:message key="usersManagement.editRole"/>:
	        <br><input type="radio" name="role" value="CLIENT"
	        <c:if test="${user.role eq 'CLIENT'}">checked</c:if>
	        > <fmt:message key="signup.client"/>&nbsp;
		    <input type="radio" name="role" value="ADMINISTRATOR"
		    <c:if test="${user.role eq 'ADMINISTRATOR'}">checked</c:if>
		    > <fmt:message key="signup.admin"/>
	      </fieldset>
	      <input type="hidden" name="userID" value="${user.userID}">
	      <button name="command" value="edit_user" style="width:100px"><fmt:message key="usersManagement.submitButton"/></button>
	    </form><br>
	    <font color="green">${editedMessage} ${deletedMessage}</font>
	    <font color="red">${editedError}</font>
      </div>
    </div>
  </div>
  
  <div id="footer">
    <%@include file="/jsp/common/footer.jspf" %>
  </div>
  
  <c:remove var="orderID" scope="session" />
  <ctg:removeMessages/>
</body>
</html> 