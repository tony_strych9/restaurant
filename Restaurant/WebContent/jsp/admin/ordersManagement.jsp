<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session" />
<fmt:setBundle basename="resources.pagecontent"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/common.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigationBar.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/manageBar.css">
<title><fmt:message key="ordersManagement.title"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript">
function setOrderID(elementId) {
	var e = document.getElementById("ordersList");
	var strUser = e.options[e.selectedIndex].value;
	document.getElementById(elementId).value = strUser;
}
function submitForm(){
    var selectButton = document.getElementById( 'infoButton' );
    selectButton.click();
}
</script>
<c:remove var="orderID" scope="session" />
</head>

<body>
  <div id="container">
    <div id="header">
      <%@include file="/jsp/common/header.jspf" %>
    </div>

    <div id="nav">
      <%@ include file="/jsp/common/navigationBar.jspf" %>
    </div>

    <div id="mbar">
      <%@ include file="/jsp/admin/manageBar.jspf" %>
    </div>

    <c:set var="counter" value="1"/>
    <div id="content">
      <div id="left-content">
        <p id="caption"><fmt:message key="ordersManagement.ordersList"/></p>
  	    <form action="${pageContext.request.contextPath}/controller" method="POST">
	      <center>
		    <div style="width:200x;">
		      <select id="ordersList" name="orderID" size="15" style="width: 500px;" onchange="submitForm()">
			    <c:if test="${empty orders}">
			      <option disabled="disabled"><fmt:message key="ordersManagement.emptyList"/>
			    </c:if>
			    <c:forEach var="order" items="${orders}">
				  <option value="${order.orderID}"
				    <c:if test="${order.orderID eq orderID}">selected</c:if>
				    >
				    <fmt:message key="ordersManagement.order"/> #${order.orderID} 
				    [<fmt:message key="ordersManagement.submitted"/>: <fmt:formatDate type="both" 
                    value="${order.time}"/>] - 
				    <c:choose>
				    <c:when test="${'CONFIRMED' eq order.status}">
				      <fmt:message key="ordersManagement.payPending"/>
				    </c:when>
				    <c:when test="${'SUBMITTED' eq order.status}">
				      <fmt:message key="ordersManagement.confirmPending"/>
				    </c:when>
				    <c:when test="${'CANCELED' eq order.status}">
				      <fmt:message key="ordersManagement.canceled"/>
				    </c:when>
				    </c:choose>
				    <c:set var="counter" value="${counter + 1}"/>
				  </option>
			    </c:forEach>
			  </select>
		    </div>
		  </center><br>
		  <center>
		    <input type="hidden" name="pageName" value=<ctg:pageName/>>
		    <button name="command" value="confirm_order" style="width:100px">
		    <fmt:message key="ordersManagement.confirmButton"/></button>
		    <button name="command" value="delete_order" style="width:100px">
		    <fmt:message key="ordersManagement.deleteButton"/></button>
		  </center><br>
		  <font color="red">${inactiveError} ${emptyError} ${canceledError} ${deletedError} ${unpaidError} ${paidError}</font>
		  <font color="green">${confirmedMessage} ${deletedMessage}</font>
	    </form>
	    <form action="${pageContext.request.contextPath}/controller">
	      <input type="hidden" name="pageName" value=<ctg:pageName/>>
	      <input type="hidden" name="orderID" id="orderIDInfo" >
		  <button id="infoButton" name="command" value="show_order_info" style="display:none;" onclick="setOrderID('orderIDInfo')"></button>
	    </form>
      </div>
      
      <div id="right-content">
        <c:if test="${orderInfo != null}">
          <p id="caption"><fmt:message key="ordersManagement.orderInfo"/></p>
          <p><textarea rows="15" style="width:350px;" readonly>${orderInfo}</textarea></p><br>
          <form action="${pageContext.request.contextPath}/controller">
          	<input type="hidden" name="pageName" value=<ctg:pageName/>>
	        <input type="hidden" name="orderID" id="orderIDToEdit" >
	        <c:if test="${requestScope.status eq 'SUBMITTED'}">
	          <button name="command" value="order_editor" style="width:100px" onclick="setOrderID('orderIDToEdit')">
		      <fmt:message key="ordersManagement.editButton"/></button>
		    </c:if>
		    <button name="command" value="manage_users" style="width:100px" onclick="setOrderID('orderIDToEdit')">
		    <fmt:message key="ordersManagement.userInfoButton"/></button>
          </form>
        </c:if>
      </div>
    </div>
  </div>
  
  <div id="footer">
    <%@include file="/jsp/common/footer.jspf" %>
  </div>
  
  <ctg:removeMessages/>
</body>
</html>  