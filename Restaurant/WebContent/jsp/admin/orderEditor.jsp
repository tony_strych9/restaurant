<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session" />
<fmt:setBundle basename="resources.pagecontent"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/common.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigationBar.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/manageBar.css">
<title><fmt:message key="orderEditor.title"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
  <c:set var="context" value="orderEditor"/>

  <div id="container">
    <div id="header">
      <%@include file="/jsp/common/header.jspf" %>
    </div>

    <div id="nav">
      <%@ include file="/jsp/common/navigationBar.jspf" %>
    </div>

    <div id="mbar">
      <%@ include file="/jsp/admin/manageBar.jspf" %>
    </div>

    <div id="content">
      <div id="left-content">
        <form action="${pageContext.request.contextPath}/controller">
          <input type="submit" value="&#8678;&nbsp;&nbsp; <fmt:message key="orderEditor.toOrdersButton"/>" style="width:100px">
          <input type="hidden" name="command" value="manage_orders">
        </form><br><br>
        <p id="caption"><fmt:message key="orderEditor.editOrder"/> #${orderID}:</p>
 	    <%@include file="/jsp/common/order.jspf" %><br>
 	    <font color="green">${deletedMessage}</font>
 	    <font color="red">${deletedError}</font>
      </div>
  
      <div id="right-content">
	    <%@ include file="/jsp/common/menu.jspf" %>
	    <font color="green">${dishAddedMessage}</font>
      </div>
    </div>
  </div>
  
  <div id="footer">
    <%@include file="/jsp/common/footer.jspf" %>
  </div>
  
  <ctg:removeMessages/>
</body>
</html>  