<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.language}" scope="session" />
<fmt:setBundle basename="resources.pagecontent"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/common.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigationBar.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/manageBar.css">
<title><fmt:message key="menuManagement.title"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
label {
	padding-left:0px;
	color:red;
}
fieldset {
	padding:15px 15px 15px 15px;
}
fieldset p {
	padding-bottom:10px;
}
fieldset legend {
	font-size: 22px;
}
</style>
</head>

<body>
  <div id="container">
  
    <div id="header">
      <%@include file="/jsp/common/header.jspf" %>
    </div>

    <div id="nav">
      <%@ include file="/jsp/common/navigationBar.jspf" %>
    </div>

    <div id="mbar">
      <%@ include file="/jsp/admin/manageBar.jspf" %>
    </div>

    <div id="content">
      <div id="left-content">
        <form action="${pageContext.request.contextPath}/controller" method="POST">
          <input type="hidden" name="command" value="show_inactive_dishes">
          <input type="checkbox" name="showInactiveBox" value="1" onchange="this.form.submit()" 
          <c:if test="${sessionScope.showInactiveBox != null}">checked</c:if>
          ><fmt:message key="menuManagement.showInactive"/><br><br>
        </form>
        <c:set var="counter" value="1"/>
        <form action="${pageContext.request.contextPath}/controller">
          <p id="caption"><fmt:message key="menuManagement.menuSections"/></p>
	      <select name="sectionID" size="10" style="width: 300px;" onchange="this.form.submit()">
		    <c:forEach var="section" items="${sections}">
		      <option value="${section.sectionID}"
			  <c:if test="${section.sectionID eq sectionID}">selected</c:if>
			  >${counter}. ${section.title}</option>
			  <c:set var="counter" value="${counter + 1}"/>
		    </c:forEach>
	      </select>
	      <input type="hidden" name="pageName" value=<ctg:pageName/>>
	      <input type="hidden" name="command" value="select_section">
        </form><br>
      
	    <form action="${pageContext.request.contextPath}/controller" method="POST" 
	    enctype="multipart/form-data" accept-charset="UTF-8">
	      <fieldset>
	        <legend><fmt:message key="menuManagement.createDish"/></legend><br>
	      
	        <p><fmt:message key="menuManagement.name"/>:
		    <label><sup>${emptyCreateNameError} ${createNameRegexError} ${createNameUsedError}</sup></label><br>
	        <input type="text" name="dishName"></p>
	      
	        <p><fmt:message key="menuManagement.price"/> ($):
		    <label><sup>${emptyCreatePriceError} ${createPriceRegexError}</sup></label><br>
	        <input type="text" name="dishPrice"></p>
	      
	        <p><fmt:message key="menuManagement.description"/>:
		    <label><sup>${createDescriptionRegexError}</sup></label><br>
	        <textarea rows="5" cols="50" name="dishDescription"></textarea><br><br></p>
	        <p><fmt:message key="menuManagement.imageFile"/>:
	        <label><sup>${createImageRegexError}${emptyCreateImageError}</sup></label></p>
	        <p><input type="file" name="imageFile" /></p>
	      </fieldset>
	      <input type="hidden" name="sectionID" value="${sectionID}">
	      <input type="submit" value=<fmt:message key="menuManagement.createButton"/> style="width:100px">
	      <input type="hidden" name="command" value="create_dish"><br><br>
	    </form><br><br>
	    <font color="green">${dishCreatedMessage}</font><br>
      </div>
    
      <div id="right-content"><br><br>
	    <c:set var="counter" value="1"/>
	    <form action="${pageContext.request.contextPath}/controller">
	      <p id="caption"><fmt:message key="menuManagement.dishesList"/></p>
		  <select name="dishID" size="10" style="width: 300px;" onchange="this.form.submit()">
            <c:forEach var="dish" items="${dishes}">
              <option value="${dish.dishID}"
		        <c:if test="${dish.dishID eq dishID}">selected</c:if>
		        >${counter}. ${dish.name} 
		        <c:if test="${'INACTIVE' eq dish.status}">
		        [<fmt:message key="menuManagement.inactive"/>]</c:if>
		      </option>
		      <c:set var="counter" value="${counter + 1}"/>
		    </c:forEach>
		  </select>
		  <input type="hidden" name="sectionID" value="${sectionID}">
		  <input type="hidden" name="pageName" value=<ctg:pageName/>>
		  <input type="hidden" name="command" value="show_dish_info" >
	    </form><br>
	  
	    <form action="${pageContext.request.contextPath}/controller" method="POST" 
	    enctype="multipart/form-data" accept-charset="UTF-8">
	      <fieldset>
	        <legend><fmt:message key="menuManagement.editDish"/></legend>
	      
	        <br><p><fmt:message key="menuManagement.editName"/>: 
		    <label><sup>${emptyEditNameError} ${editNameRegexError} ${editNameUsedError}</sup></label><br>
	        <input type="text" name="dishName" value="${dish.name}"></p>
	       
	        <p><fmt:message key="menuManagement.editPrice"/> ($):
		    <label><sup>${emptyEditPriceError} ${editPriceRegexError}</sup></label><br>
	        <input type="text" name="dishPrice" value="${dish.price}"></p>
	      
	        <p><fmt:message key="menuManagement.editDescription"/>:
		    <label><sup>${editDescriptionRegexError}</sup></label><br>
	        <textarea rows="5" cols="50" name="dishDescription">${dish.description}</textarea></p>
	      
	        <p><input type="radio" name="dishStatus" value="ACTIVE" 
	        <c:if test="${'ACTIVE' eq dish.status}">checked</c:if>
	        ><fmt:message key="menuManagement.active"/>
	        <input type="radio" name="dishStatus" value="INACTIVE"
	        <c:if test="${'INACTIVE' eq dish.status}">checked</c:if>
	        ><fmt:message key="menuManagement.inactive"/></p><br>
	        <p><fmt:message key="menuManagement.editImage"/>:
	        <label><sup>${editImageRegexError}</sup></label></p>
	        <img src="http://${pageContext.request.serverName}:${pageContext.request.serverPort}/images/${dish.image}" 
	        style="width:300px;height:230px;"><br><br>
	        <p><input type="file" name="imageFile"/></p>
	      </fieldset>
	      <input type="hidden" name="dishID" value="${dish.dishID}">
	      <input type="submit" value=<fmt:message key="menuManagement.submitButton"/> style="width:100px">
	      <input type="hidden" name="command" value="edit_dish"><br><br>
	    </form>
	    <font color="green">${dishChangedMessage}</font>
	  </div>
    </div>
  </div>
  
  <div id="footer">
    <%@include file="/jsp/common/footer.jspf" %>
  </div>
  
  <ctg:removeMessages/>
</body>
</html>  